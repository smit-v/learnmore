const video = document.getElementById('video')
document.getElementById('startBTN').style.display = "none";

Promise.all([
  faceapi.nets.tinyFaceDetector.loadFromUri('/models'),
  faceapi.nets.faceLandmark68Net.loadFromUri('/models'),
  faceapi.nets.faceRecognitionNet.loadFromUri('/models'),
  faceapi.nets.ssdMobilenetv1.loadFromUri('/models'),
]).then(startVideo)

function startVideo() {
  navigator.getUserMedia(
    { video: {} },
    stream => video.srcObject = stream,
    err => console.error(err)
  )
}

video.addEventListener('play', () => {
  const canvas = faceapi.createCanvasFromMedia(video)
  document.body.append(canvas)
  const displaySize = { width: video.width, height: video.height }
  faceapi.matchDimensions(canvas, displaySize)

  let labeledFaceDescriptors
  (async () => {
    labeledFaceDescriptors = await loadLabeledImages()
  })()

  MyInterval = setInterval(async () => {
    const detections = await faceapi.detectAllFaces(video, new faceapi.TinyFaceDetectorOptions()).withFaceLandmarks().withFaceDescriptors()
    const resizedDetections = faceapi.resizeResults(detections, displaySize)
    canvas.getContext('2d').clearRect(0, 0, canvas.width, canvas.height)

    if (labeledFaceDescriptors) {
      const faceMatcher = new faceapi.FaceMatcher(labeledFaceDescriptors, 0.6)
      const results = resizedDetections.map(d => faceMatcher.findBestMatch(d.descriptor))
      results.forEach((result, i) => {
        const box = resizedDetections[i].detection.box
        const drawBox = new faceapi.draw.DrawBox(box, { label: result.toString() })
        drawBox.draw(canvas)

        if(!result.toString().includes('unknown'))
        {
          stop(result.label);
        }
      })
    }

  }, 1000)
})

function stop(userrecog)
{
  clearInterval(MyInterval);
  console.log(userrecog);

  $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
        });
        $.ajax({
            type: "POST",
            url: 'http://localhost:8000/quiz/attendance/'+document.getElementById('qaid').value,

            data: {user: userrecog},
            success: function( data ) {
                // $("#ajaxResponse").innerHTML = data ;
                // console.log(data);
            },
            statusCode: {
                200: function(){
                         console.log("200")
                     },
                500: function(){
                         console.log("500")
                     }
            }
        });

        document.getElementById('startBTN').style.display = "block";
}

function loadLabeledImages() {
  var str = document.getElementById('userids').value.replace("[","");
  var str = str.replace("]","");
//   console.log(str);
  var arr = str.split(",");
  const labels = arr;
//   console.log(labels);
  return Promise.all(
    labels.map(async label => {
      const descriptions = []
      for (let i = 1; i <= 1; i++) {
        // const img = await faceapi.fetchImage(`https://raw.githubusercontent.com/naitik2314/Face-Recognition-JavaScript/master/labeled_images/${label}/${i}.jpg`)
        // const img = await faceapi.fetchImage(`labeled_images/${label}/${i}.jpg`)
        const img = await faceapi.fetchImage(`http://localhost:8000/storage/users/${label}/${i}.jpg`)
        const detections = await faceapi.detectSingleFace(img).withFaceLandmarks().withFaceDescriptor()
        descriptions.push(detections.descriptor)
      }

      return new faceapi.LabeledFaceDescriptors(label, descriptions)
    })
  )
}
