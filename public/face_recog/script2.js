const video = document.getElementById('video')
var count = 0;
Promise.all([
  faceapi.nets.tinyFaceDetector.loadFromUri('/models'),
  faceapi.nets.faceLandmark68Net.loadFromUri('/models'),
  faceapi.nets.faceRecognitionNet.loadFromUri('/models'),
  faceapi.nets.ssdMobilenetv1.loadFromUri('/models'),
]).then(startVideo)

function startVideo() {
  navigator.getUserMedia(
    { video: {} },
    stream => video.srcObject = stream,
    err => console.error(err)
  )
}

function stopVideo()
{
    video.pause();
    clearInterval(Myint);
    navigator.getUserMedia(
        { video: {} },
        stream => null,
        err => console.error(err)
      )
}

video.addEventListener('play', () => {
  const canvas = faceapi.createCanvasFromMedia(video)
  document.body.append(canvas)
  const displaySize = { width: video.width, height: video.height }
  faceapi.matchDimensions(canvas, displaySize)

  let labeledFaceDescriptors
  (async () => {
    labeledFaceDescriptors = await loadLabeledImages()
  })()

  Myint = setInterval(async () => {
    const detections = await faceapi.detectAllFaces(video, new faceapi.TinyFaceDetectorOptions()).withFaceLandmarks().withFaceDescriptors()
    var len = detections.length;
    var l = 20-count;
    if(len >= 2)
    {

      document.getElementById("video_message").innerHTML= "Not more than 1 people allowed in the frame. Test will get submitted in "+l+"secs.";
      count++;
      if(count >= 20)
      {
          stopVideo();
          document.getElementById("video-div").style.display = "none";
          document.getElementsByTagName('canvas')[0].style.display = "none";
          submitTest();
      }

    }
    else
    {
      document.getElementById("video_message").innerHTML= "Tolerance Time only"+l+"sec. Else will be submiited";
    }
    const resizedDetections = faceapi.resizeResults(detections, displaySize)

    canvas.getContext('2d').clearRect(0, 0, canvas.width, canvas.height)

    if (labeledFaceDescriptors) {
      const faceMatcher = new faceapi.FaceMatcher(labeledFaceDescriptors, 0.6)
      const results = resizedDetections.map(d => faceMatcher.findBestMatch(d.descriptor))
      results.forEach((result, i) => {
        const box = resizedDetections[i].detection.box
        const drawBox = new faceapi.draw.DrawBox(box, { label: "" })
        drawBox.draw(canvas)
      })
    }

  }, 1000)
})

function loadLabeledImages() {
  const labels = ['black widow', 'captain america', 'captain marvel', 'hawkeye', 'jim rhodes', 'thor', 'tony stark', 'naitik']
  return Promise.all(
    labels.map(async label => {
      const descriptions = []
      for (let i = 1; i <= 1; i++) {
        // const img = await faceapi.fetchImage(`https://raw.githubusercontent.com/naitik2314/Face-Recognition-JavaScript/master/labeled_images/${label}/${i}.jpg`)
        const img = await faceapi.fetchImage(`http://localhost:8000/storage/labeled_images/${label}/${i}.jpg`)
        const detections = await faceapi.detectSingleFace(img).withFaceLandmarks().withFaceDescriptor()
        descriptions.push(detections.descriptor)
      }

      return new faceapi.LabeledFaceDescriptors(label, descriptions)
    })
  )
}
