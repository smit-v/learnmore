<?php

namespace App\Notifications;

use App\Models\quizeattempt;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class StudentReexam extends Notification
{
    use Queueable;

    private quizeattempt $quizeattempt;
    public function __construct(quizeattempt $quizeattempt)
    {
        $this->quizeattempt = $quizeattempt;
    }


    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $code = $this->quizeattempt->quiz->quizcode;
        return (new MailMessage)
                    ->line('You been Given chance to re attempt the quiz')
                    ->line('Use: '. $code . ' to enter to the quiz again')
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'quiz' => $this->quizeattempt->quiz->title,
            'quizcode' => $this->quizeattempt->quiz->quizcode
        ];
    }
}
