<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class quiz extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    public function assignedBy()
    {
        return $this->belongsTo(User::class, 'assigned_by');
    }

    public function quizquestion()
    {
        return $this->belongsToMany(quizquestion::class, 'quiz_quizquestion');
    }

    public function checkOngoing()
    {
        $quizDate = $this->submission_date;
        $quizTime = $this->submission_time;
        $now = Carbon::now();
        if($now > $quizDate)
        {
            return false;
        }
        else
        {
            if($now->format('y-m-d') == $quizDate && $now->format('H:i:s') < $quizTime )
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    public function quizattempts()
    {
        return $this->hasMany(quizeattempt::class, 'quiz_id');
    }
}
