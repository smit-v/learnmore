<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class quizquestion extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    public function questionedby()
    {
        return $this->belongsTo(User::class, 'question_by');
    }
}
