<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Storage;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'organization',
        'type',
        'image1',
        'image2',
        'approved'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [

    ];


    public function isStudent()
    {
        return $this->type == "Student";
    }

    public function isAdmin()
    {
        return $this->type == "Admin";
    }

    public function isApproved()
    {
        return $this->approved == "YES";
    }

    public function isTeacher()
    {
        return $this->type == "Teacher";
    }

    //Relationship Methods
    public function questions()
    {
        return $this->hasMany(Question::class);
    }

    public function answers()
    {
        return $this->hasMany(Answer::class);
    }

    public function quizzes()
    {
        return $this->hasMany(quiz::class, 'assigned_by');
    }

    public function quizAttempts()
    {
        return $this->hasMany(quizeattempt::class, 'student_id');
    }

    // public function favorites()
    // {
    //     return $this->belongsToMany(Question::class)->withTimestamps();
    // }
    public function toAnswer()
    {
        return $this->belongsToMany(Question::class, 'question_user');
    }

    public function assigned()
    {
        return $this->belongsToMany(Assignment::class, 'user_assignment');
    }

    public function votesQuestions()
    {
        return $this->morphedByMany(Question::class, 'vote')->withTimestamps();
    }
    public function votesAnswers()
    {
        return $this->morphedByMany(Answer::class, 'vote')->withTimestamps();
    }

    public function hasQuestionUpVote(Question $question)
    {
        return auth()->user()->votesQuestions()->where(['vote' => 1, 'vote_id' => $question->id, 'vote_type' => Question::class])->exists();
    }
    public function hasQuestionDownVote(Question $question)
    {
        return auth()->user()->votesQuestions()->where(['vote' => -1, 'vote_id' => $question->id, 'vote_type' => Question::class])->exists();
    }
    public function hasVoteForQuestion(Question $question)
    {
        return $this->hasQuestionUpVote($question) || $this->hasQuestionDownVote($question);
    }


    public function hasAnswerUpVote(Answer $answer)
    {
        return auth()->user()->votesAnswers()->where(['vote' => 1, 'vote_id' => $answer->id, 'vote_type' => Answer::class])->exists();
    }
    public function hasAnswerDownVote(Answer $answer)
    {
        return auth()->user()->votesAnswers()->where(['vote' => -1, 'vote_id' => $answer->id, 'vote_type' => Answer::class])->exists();
    }
    public function hasVoteForAnswer(Answer $answer)
    {
        return $this->hasAnswerUpVote($answer) || $this->hasAnswerDownVote($answer);
    }

    public function getAvatarAttribute()
    {
        $size = 40;
        $name = $this->name;
        return "https://ui-avatars.com/api/?name={$name}&rounded=true&size={$size}";
    }

    public function getUrl1Attribute()
    {
        return "users/{$this->id}/1.jpg";
    }

    public function getUrl2Attribute()
    {
        return "users/{$this->id}/2.jpg";
    }

    public function scopeSearch($query)
    {
        $search = request('search');
        if($search)
        {
            return $query->where('name','like',"%$search%");
        }
        return $query;
    }

    public function deleteImage1()
    {
        Storage::delete($this->image1);
    }

    public function deleteImage2()
    {
        Storage::delete($this->image2);
    }

    public function getImagePath1Attribute()
    {
        return 'storage/'.$this->image1;
    }

    public function getImagePath2Attribute()
    {
        return 'storage/'.$this->image2;
    }
}
