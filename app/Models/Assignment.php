<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Assignment extends Model
{
    use HasFactory;
    protected $guarded = [''];

    public function assigned()
    {
        return $this->belongsToMany(User::class, 'user_assignment');
    }

    public function getRefPathAttribute()
    {
        return 'storage/'.$this->ref;
    }

    public function scopeSearchuser($query)
    {
        return $query->where('user_id',auth()->id());
    }

    public function deleteDoc()
    {
        Storage::delete($this->ref);
    }
}
