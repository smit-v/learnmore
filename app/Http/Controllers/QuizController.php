<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateAttemptRequest;
use App\Http\Requests\CreateQuizRequest;
use App\Http\Requests\UpdateQuizAttemptRequest;
use App\Http\Requests\UpdateQuizRequest;
use App\Models\quiz;
use App\Models\quizeattempt;
use App\Models\quizquestion;
use App\Models\User;
use App\Notifications\StudentReexam;
use Carbon\Carbon;
use Illuminate\Http\Request;

class QuizController extends Controller
{
    public function startQuiz($id)
    {
        $q = quizeattempt::findOrFail($id);
        if($q->status == "ONGOING")
        {
            $quiz = quiz::findOrFail($q->quiz_id);
            $quizQuestions = $quiz->quizquestion;
            $user = auth()->user();
            $time = 0;
            if($q->startTime)
            {
                $time = $q->startTime;
            }
            else
            {
                $time = $quiz->duration*60;
            }
            return view('quiz.index1', compact(['quizQuestions', 'quiz', 'user', 'q', 'time']));
        }
        else
        {
            return abort(419,'Unwell activity contact teacher');
        }
    }

    public function showAttendancePage($id)
    {
        $qa = quizeattempt::findOrFail($id);
        $users = User::where('type','Student')->whereNotNull('image1')->get();
        $ids = $users->pluck('id');

        $now = Carbon::now();
        if($now->format('Y-m-d') <= $qa->quiz->submission_date)
        {
            if($now->format('h:i:s') <= $qa->quiz->submission_time)
            {
                return view('quiz.attendance', compact(['qa', 'ids']));
            }
            else
            {
                abort(419, "Quiz is Expired");
            }
        }
        else
        {
            abort(419, "Quiz is Expired");
        }

    }

    public function markAttendance($id)
    {
        $qa = quizeattempt::findOrFail($id);
        if(request()->user == $qa->student_id)
        {
            $qa->update(['status' => 'ONGOING']);
        }
        else if($qa->status == "LOGGEDIN")
        {
            $qa->update(['status'=>'LOGGEDIN-2']);
            return abort(419,'You have failed Facial Recog, One chance remaining');
        }
        else if($qa->status == "LOGGEDIN-2")
        {
            return abort(419,'You have failed Facial Recog, Contact Teacher');
        }
    }

    public function studentCode(CreateAttemptRequest $request)
    {
        // Check whether the code is valid?
        $quiz = quiz::where('quizcode', $request->quizCode)->first();
        $user = auth()->user();
        $now = Carbon::now();

        $q = quizeattempt::where('quiz_id',$quiz->id)->where('student_id',$user->id)->first();
        if($q)
        {
            if($q->status = "REEXAM")
            {
                return redirect(route('quiz.showAttendancePage', $q));
            }
            else
            {
                $quizQuestions = $quiz->quizquestion;
                return abort(419,'You Have Already Responded to the Quiz. Please Contact Teacher if any difficulty faced');
            }
        }
        else
        {
            $q = quizeattempt::create([
                'quiz_id' => $quiz->id,
                'student_id' => $user->id,
                'status' => 'LOGGEDIN',
                'startTime' => NULL,
            ]);
            // $quizQuestions = $quiz->quizquestion;
            // return view('quiz.index1', compact(['quizQuestions', 'quiz', 'user', 'q']));
            return redirect(route('quiz.showAttendancePage', $q));

        }
    }

    public function showQuizHome()
    {
        $quizQuestions = quizquestion::all();
        return view('quiz.quizhome', compact(['quizQuestions']));
    }

    public function store(CreateQuizRequest $request)
    {
        $quizcode = "";
        while(true)
        {
            $length = 8;
            $str = '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcefghijklmnopqrstuvwxyz';
            $quizcode = substr(str_shuffle($str), 0, $length);
            $check = quiz::where('quizcode',$quizcode)->first();
            if(!$check)
            {
                break;
            }
        }

        $quiz = quiz::create([
            'title' => $request->title,
            'submission_date' => $request->date,
            'submission_time' => $request->time,
            'outoff_marks' => $request->outoff_marks,
            'quizcode' => $quizcode,
            'duration' => $request->duration,
            'assigned_by' => auth()->user()->id,
            'show_results' => $request->results,
        ]);

        return redirect(route('quiz.teacher.questionshow', $quiz));
    }

    public function questionsShow($q)
    {
        $quiz = quiz::findOrFail($q);
        $quizQs = $quiz->quizquestion;
        $quizQuestions = quizquestion::all();
        return view('quiz.question', compact(['quiz', 'quizQuestions', 'quizQs']));
    }

    public function questionUp($q)
    {
        $quiz = quiz::findOrFail($q);
        $question = quizquestion::findOrFail(request()->quizquestion_id);
        $quiz->quizquestion()->attach($question);
        $quizQs = $quiz->quizquestion;
        $quizQuestions = quizquestion::all();
        return view('quiz.question', compact(['quiz', 'quizQuestions', 'quizQs']));
    }

    public function questionDown($q)
    {
        $quiz = quiz::findOrFail($q);
        $question = quizquestion::findOrFail(request()->quizquestion_id);
        $quiz->quizquestion()->detach($question);
        $quizQs = $quiz->quizquestion;
        $quizQuestions = quizquestion::all();
        return view('quiz.question', compact(['quiz', 'quizQuestions', 'quizQs']));
    }

    public function showManageQuiz(User $user)
    {
        $quizzes = $user->quizzes;
        return view('quiz.manageQuiz', compact(['user', 'quizzes']));
    }

    public function endQuiz(UpdateQuizAttemptRequest $request)
    {
        //add check on date and time.
        $qa = quizeattempt::findOrFail($request->qa);

        $now = Carbon::now();
        if($now->format('Y-m-d') <= $qa->quiz->submission_date)
        {
            if($now->format('h:i:s') <= $qa->quiz->submission_time)
            {
                $qa->update([
                    'marks' => $request->marks,
                    'status' => "SUBMITTED"
                ]);

                return view('dashboard');
            }
            else
            {
                abort(419, "Quiz is Expired");
            }
        }
        else
        {
            abort(419, "Quiz is Expired");
        }

    }

    public function edit($user,$id)
    {
        $quiz = quiz::findOrFail($id);
        return view('quiz.edit', compact(['quiz']));
    }

    public function update(UpdateQuizRequest $request)
    {
        $quiz = quiz::findOrFail($request->id);
        $quiz->update([
            'title' => $request->title,
            'submission_date' => $request->date,
            'submission_time' => $request->time,
            'outoff_marks' => $request->marks,
            'duration' => $request->duration,
            'show_results' => $request->show_results
        ]);
        return view('quiz.edit', compact(['quiz']));
    }

    public function delete($user, $id)
    {
        $quiz = quiz::findOrFail($id);
        $quiz->delete();

        return redirect(route('quiz.teacher.manageQuiz', $user));
    }

    public function reexam()
    {
        $attempt = quizeattempt::findOrFail(request()->attempt_id);

        $attempt->student->notify(new StudentReexam($attempt));

        $attempt->update(['status' => 'REEXAM', 'marks' => NULL]);
        return redirect(route('quiz.teacher.edit.quiz', [$attempt->quiz->assigned_by, $attempt->quiz->id]));
    }

    public function updateTime()
    {
        $qa = quizeattempt::findOrFail(request()->qa);
        $qa->update(['startTime' => request()->startTime]);
    }

    public function late()
    {
        $qa = quizeattempt::findOrFail(request()->qa);
        $qa->update(['status' => "LATE"]);
    }
}
