<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateQuestionRequest;
use App\Http\Requests\UpdateQuestionRequest;
use App\Models\Question;
use App\Models\User;
use App\Notifications\NewQuestionAsked;
use Illuminate\Http\Request;


class QuestionsController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth'])->except('index','show');
    }

    public function index()
    {
        $questions = Question::search()->with('owner')->simplePaginate(6);
        return view('questions.index',compact(['questions']));
    }

    public function create()
    {
        // app('debugbar')->disable();
        $teachers = User::where('type','Teacher')->get();
        // dd($teachers);
        return view('questions.create', compact(['teachers']));
    }

    public function store(CreateQuestionRequest $request)
    {


        $question = auth()->user()->questions()->create([
            'title' => $request->title,
            'body' => $request->body
        ]);

        $question->askedTo()->attach($request->teachers);

        foreach($question->askedTo as $teacher)
        {
            $teacher->notify(new NewQuestionAsked($question));
        }


        session()->flash('success', 'Question Added!');
        return redirect(route('questions.index'));
    }

    public function show(Question $question)
    {
        $question->increment('views_count');
        return view('questions.show',compact(['question']));
    }

    public function showById()
    {
        $question = Question::searchId()->first();

        $question->increment('views_count');

        return view('questions.show',compact(['question']));
    }

    public function edit(Question $question)
    {
        if($this->authorize('update',$question)){
            return view('questions.edit', compact(['question']));
        }
        abort(403,'Access Denied.');
    }

    public function update(UpdateQuestionRequest $request, Question $question)
    {
        if($this->authorize('update',$question)){
            $question->update([
                'title' => $request->title,
                'body' => $request->body
            ]);

            session()->flash('success', 'Question Edited!');
            return redirect(route('questions.index'));
        }
        abort(403,'Access Denied.');
    }

    public function destroy(Question $question)
    {
        if($this->authorize('delete',$question)){
            $question->delete();

            session()->flash('success', 'Question Deleted!');
            return redirect(route('questions.index'));
        }
        abort(403);
    }
}
