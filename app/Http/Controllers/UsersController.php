<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateUser;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class UsersController extends Controller
{

    public function adminIndex()
    {
        $users = User::search()->simplePaginate(5);
        return view('admin.index', compact('users'));
    }

    public function adminUpdate(UpdateUser $request, User $user)
    {
        $user->update([
            'name' => $request->name,
            'organization' => $request->organization,
            'type' => $request->type,
            'email' => $request->email
        ]);

        return redirect(route('admin.show', $user->id));
    }

    public function adminDelete(User $user)
    {
        $user->deleteImage1();
        $user->deleteImage2();
        $user->delete();
        return redirect(route('admin.index'));
    }

    public function adminApprove(User $user)
    {
        $user->update(['approved' => 'YES']);
        return redirect(route('admin.show', $user->id));
    }

    public function adminDeleteImage(User $user)
    {

        $user->deleteImage1();
        $user->deleteImage2();
        $user->update([
            'image1' => NULL,
            'image2' => NULL
        ]);

        return redirect(route('admin.show', $user->id));
    }



    public function adminShow(User $user)
    {
        return view('admin.show', compact(['user']));
    }

    public function notifications()
    {
        auth()->user()->unreadNotifications->markAsRead();
        $notifications = auth()->user()->notifications()->get();
        return view('users.notification', compact(['notifications']));
    }

    public function userPhoto()
    {
        $user = auth()->user();
        $HiddenString = rand(1,2000);
        return view('users.photoclick', compact(['user', 'HiddenString']));
    }

    public function userPhotoUplaod()
    {

        // $fileName = 1 . '.png';

        // $file = $folderPath . $fileName;
        // file_put_contents($file, $image_base64);

        if(request()->photo)
        {
            $img = request()->photo;
            $id = auth()->user()->id;
            $user = User::findOrFail($id);

            $image_parts = explode(";base64,", $img);
            // $image_type_aux = explode("image/", $image_parts[0]);
            // $image_type = $image_type_aux[1];

            $myFile = base64_decode($image_parts[1]);

            $userId = $user->id;
            if(!$user->image1)
            {
                // $image = $myFile->storeAs("users/". $user->id, "1.jpg");
                $filePath = 'users/'.$userId.'/1.jpg';
                Storage::disk('public')->put($filePath, $myFile);
                // dd($user);
                $user->update(['image1' => $filePath]);
            }
            else if(!$user->image2)
            {
                $filePath = 'users/'.$userId.'/2.jpg';
                Storage::disk('public')->put($filePath, $myFile);
                $user->update(['image2' => $filePath]);
            }
        }
        return redirect(route('dashboard'));
    }

}
