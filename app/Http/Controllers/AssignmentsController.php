<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateAssigmentRequest;

use App\Http\Requests\UpdateAssignmentRequest;
use App\Http\Requests\UpdateStudentAssignment;
use App\Http\Requests\UpdateTeacherAssignment;
use App\Models\Assignment;
use App\Models\UserAssignment;
use App\Notifications\AssignmentChanged;
use App\Notifications\AssignmentReminder;
use App\Notifications\AssignmentReturned;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class AssignmentsController extends Controller
{

    public function notifyStudents(Assignment $assignment)
    {
        $userassignments = UserAssignment::with('assignment','assignedTo')->where('assignment_id', $assignment->id)->get();
        foreach($userassignments as $userassignment)
        {
            if(!$userassignment->submission_date)
            {
                $userassignment->assignedTo->notify(new AssignmentReminder($assignment));
            }
        }
        return view('assignements.show', compact(['assignment', 'userassignments']));
    }

    public function show(Assignment $assignment)
    {
        $userassignments = UserAssignment::with('assignment', 'assignedTo')->where('assignment_id', $assignment->id)->get();
        return view('assignements.show', compact(['assignment', 'userassignments']));
    }

    public function edit(Assignment $assignment)
    {
        return view('assignements.edit', compact(['assignment']));
    }

    public function update(UpdateAssignmentRequest $request, Assignment $assignment)
    {
        $data = $request->only(['title', 'description', 'due_date', 'due_time', 'total_marks']);
        if($request->hasFile('ref'))
        {
            $ref = $request->ref->store('assignments/docs');
            $data['ref'] = $ref;
            $assignment->deleteDoc();
        }

        $assignment->update($data);

        foreach($assignment->assigned as $user)
        {
            $user->notify(new AssignmentChanged($assignment));
        }

        session()->flash('success', 'Post Updated!');
        return redirect(route('assignments.index'));
    }

    public function index()
    {
        $assignments = Assignment::searchuser()->get();
        return view('assignements.index', compact(['assignments']));
    }

    public function create()
    {
        return view('assignements.create');
    }

    public function store(CreateAssigmentRequest $request)
    {
        if($request->file('ref'))
        {
            $ref = $request->file('ref')->store('assignments/docs');
        }
        else
        {
            $ref = null;
        }


        $assignment = Assignment::create([
            'user_id' => auth()->id(),
            'title' => $request->title,
            'description' => $request->description,
            'ref' => $ref,
            'due_date' => $request->date,
            'due_time' => $request->time,
            'total_marks' => $request->total_marks
        ]);
        // session()->flash('success', 'Your answered is Added!');
        $userassignments = UserAssignment::with('assignment', 'assignedTo')->where('assignment_id', $assignment->id)->get();
        // return view('assignements.show', compact(['assignment', 'userassignments']));
        return redirect(route('assignments.show', [$assignment, $userassignments]));
    }

    public function destroy(Assignment $assignment)
    {
        $assignment->deleteDoc();
        $assignment->delete();
        session()->flash('success', 'Assignment Deleted!');
        return redirect(route('assignments.index'));
    }






    public function acceptAssignment(Request $request)
    {
        $validated = $request->validate([
            'assignment_id' => 'required|exists:assignments,id',
            'users' => 'required|exists:assignments,user_id',
        ]);

        $assignment = Assignment::where('id', $validated['assignment_id'])->first();

        if(($assignment->assigned->pluck('id')->search(auth()->id()) === 0) || ($assignment->assigned->pluck('id')->search(auth()->id()) != false))
        {
            abort(419, "already enrolled, check your assignments section");
            return;
        }

        $assignment->assigned()->attach(auth()->id());
        return redirect(route('students.assignment.index'));
    }

    public function indexStudent()
    {
        $assignments = UserAssignment::with('assignment')->where('user_id',auth()->id())->get();
        return view('assignements.indexStudent', compact(['assignments']));
    }

    public function showStudent($id)
    {
        $userassignment = UserAssignment::with('assignment')->findOrFail($id);
        return view('assignements.showStudent', compact(['userassignment']));
    }

    public function showTeacher($id)
    {
        $userassignment = UserAssignment::with('assignment')->findOrFail($id);
        return view('assignements.showTeacher', compact(['userassignment']));
    }

    public function withdraw($id)
    {
        $userassignment = UserAssignment::with('assignment')->findOrFail($id);
        //Doc to be deleted if submitted
        $userassignment->delete();

        return redirect(route('students.assignment.index'));
    }

    public function updateStudent(UpdateStudentAssignment $request, $id)
    {
        $userassignment = UserAssignment::with('assignment')->findOrFail($id);

        $submission_date = Carbon::today();
        $submission_time = Carbon::now();


        if($userassignment->assignment->due_date <= $submission_date->toDateString())
        {
            if($userassignment->assignment->due_time < $submission_time->toTimeString())
            {
                dd("You are late sorry !");
            }
        }

        if($userassignment->doc)
        {
            $userassignment->deleteDoc();
        }


        $doc = $request->file('doc')->store("assignments/docs");
        //Different folder system



        $userassignment->update([
            'submission_date' => $submission_date,
            'submission_time' => $submission_time,
            'doc' => $doc,
            'textOrfeedback' => $request->textOrfeedback
        ]);

        return redirect(route('students.assignment.show',$userassignment->id));
    }

    public function updateTeacher($id)
    {

        $userassignment = UserAssignment::with('assignment')->findOrFail($id);
        $total_marks = $userassignment->assignment->total_marks;
        request()->validate([
            'alloted_marks' => 'required|integer|min:0|max:'.$total_marks,
        ]);



        $userassignment->update([
            'alloted_marks' => request()->alloted_marks,
        ]);

        $userassignment->assignedTo->notify(new AssignmentReturned($userassignment->assignment));

        return redirect(route('teachers.assignment.show',$userassignment->id));
    }
}
