<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateQuizQuestionRequest;
use App\Models\quizquestion;
use Illuminate\Http\Request;

class QuizequestionController extends Controller
{
    public function create()
    {
        return view('quiz.questionAdd');
    }

    public function store(CreateQuizQuestionRequest $request)
    {
        quizquestion::create([
            'question' => $request->question,
            'option1' => $request->option1,
            'option2' => $request->option2,
            'option3' => $request->option3,
            'option4' => $request->option4,
            'answer' => $request->answer,
            'question_by' => auth()->user()->id
        ]);

        return redirect(route('quiz.teacher.home'));
    }

    public function edit($q)
    {
        $quizQuestion = quizquestion::findOrFail($q);
        return view('quiz.questionEdit', compact(['quizQuestion']));
    }

    public function update(CreateQuizQuestionRequest $request, $q)
    {
        $quizQuestion = quizquestion::findOrFail($q);
        $data = $request->all();
        $quizQuestion->update($data);
        return redirect(route('quiz.teacher.home'));
    }

    public function destroy($q)
    {
        $quizQuestion = quizquestion::findOrFail($q);
        $quizQuestion->delete();
        return redirect(route('quiz.teacher.home'));
    }
}
