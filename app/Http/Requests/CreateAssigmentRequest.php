<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Carbon;

class CreateAssigmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $today = Carbon::yesterday();
        // $now = Carbon::now();
        // dd(request()->time);
        return [
            'title' => 'required|string|max:256|min:5',
            'date' => 'required|date|after:'.$today,
            'description' => 'required|string',
            'time' => 'required|date_format:H:i',
            'total_marks' => 'required|min:0',
            'ref' => 'file|mimes:png,jpg,pdf,jpeg,docx'
        ];
    }
}
