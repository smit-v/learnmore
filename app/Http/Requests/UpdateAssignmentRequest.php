<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Carbon;

class UpdateAssignmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        request()->due_time = substr(request()->due_time,0,5);
        $today = Carbon::yesterday();
        // $now = Carbon::now();
        // dd(request()->time);
        return [
            'title' => 'string|max:256|min:5',
            'due_date' => 'date|after:'.$today,
            'description' => 'string',
            'due_time' => 'date_format:H:i',
            'total_marks' => 'min:0',
            'ref' => 'file|mimes:png,jpg,pdf,jpeg,docx'
        ];
    }
}
