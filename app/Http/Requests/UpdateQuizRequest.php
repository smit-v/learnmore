<?php

namespace App\Http\Requests;

use App\Models\quiz;
use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class UpdateQuizRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $quiz = quiz::findOrFail(request()->id);
        $today = Carbon::yesterday();
        return [
            'title' => 'required',
            'marks' => 'integer|required|min:0',
            'date' => 'required|date|after:'.$today,
            'duration' => 'required|integer|min:1|max:240',
            'time' => 'required|date_format:H:i',
            'show_results' => 'required|in:YES,NO',
            'quizcode' => 'required|in:'.$quiz->quizcode,
        ];
    }
}
