<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id=request()->id;
        return [
            'name' => "required|min:5",
            'organization' => "required|min:2",
            'type' => 'required|in:Teacher,Student',
            'email' => 'required|email|unique:users,email,'.$id,
        ];
    }
}
