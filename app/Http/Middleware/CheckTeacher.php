<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Http\Request;

class CheckTeacher
{

    public function handle(Request $request, Closure $next)
    {
        if(auth()->user()->isTeacher() && auth()->user()->isApproved())
        {
            return $next($request);
        }

        return abort(401, "Unathuorized");
    }
}
