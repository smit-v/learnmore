@extends('layouts.app')
@section('content')
<div class="container">
    {{-- {{ dd($questions) }} --}}
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="d-flex mb-3 justify-content-end">
                <a class="mr-3 btn btn-primary" href="{{ route('dashboard') }}">
                    Back to Dashboard
                </a>
                @if (auth()->user()->type == 'Student')
                    <a href="{{ route('questions.create') }}" class="btn btn-primary mr-3">Ask a question!</a>
                @endif
                <div class="">
                    <form action="{{ route('questions.index') }}" method="GET">
                        <input type="text" name="search" class="" placeholder="Search Title" value="{{ request('search') }}">
                        <button type="submit"class="btn btn-success"><i class="fa fa-search"></i></button>
                    </form>
                </div>
                <button type="button" class="ml-3 btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                    Search by ID
                </button>
                <a href="https://erudite-quest.herokuapp.com/" class="ml-3 btn btn-primary">
                    Search Similar Question
                </a>
            </div>
            <div class="card">
                <div class="card-header">All Questions</div>
                @foreach ($questions as $question)
                    <div class="card-body">
                        <div class="media">
                            <div class="stats mr-3">
                                <div class="votes text-center mb-3">
                                    <strong class="d-block">{{ $question->votes_count }}</strong> Votes
                                </div>
                                <div class="answers {{ $question->answer_style }} text-center mb-3">
                                    <strong class="d-block">{{ $question->answers_count }}</strong> Answers
                                </div>
                                <div class="views text-center">
                                    <strong class="d-block">{{ $question->views_count }}</strong> Views
                                </div>
                            </div>
                            <div class="media-body">
                                <div class="d-flex justify-content-between">
                                    <h4><a href="{{ $question->url }}">{{ $question->title }}</a></h4>
                                    <h6 class="float-right">Question ID: {{ $question->id }}</h6>
                                <div>
                                        @can('update',$question)
                                            <a href="{{ route('questions.edit',$question->id) }}" class="btn mb-1 btn-warning">Edit</a>
                                        @endcan

                                        @can('delete',$question)
                                            <form action="{{ route('questions.destroy',$question->id) }}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete it?')">Delete</button>
                                            </form>
                                        @endcan
                                    </div>
                                </div>
                                <p>{!! \Illuminate\Support\Str::limit($question->body, 200) !!}</p>
                            </div>
                        </div>
                    </div>
                    <hr>
                @endforeach

                <div class="card-footer">
                    {{ $questions->appends(['search' => request('search')])->links() }}
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Give Question ID</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('questions.showById') }}" method="POST">
                    @csrf
                    <input type="text" name="searchById" class="" placeholder="Question ID" value="{{ request('searchById') }}">
                    <button type="submit"class="btn btn-success"><i class="fa fa-search"></i></button>
                </form>
            </div>
          </div>
        </div>
      </div>
</div>
@endsection
