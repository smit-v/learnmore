{{-- <x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    You're logged in!
                </div>
            </div>
        </div>
    </div>
</x-app-layout> --}}


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Erudite</title>

    <!-- font awesome cdn link  -->
    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css"
    />

    <!-- custom css file link  -->
    <link rel="stylesheet" href="{{ asset('css/dashboard_blade.css') }}" />
    <style>
        .dropbtn {
          background-color: #04aa6d;
          color: white;
          padding: 16px;
          font-size: 16px;
          border: none;
        }

        .dropdown {
          position: relative;
          left: 87.696969%;
          top: 25px;
          display: inline-block;
        }

        .dropdown-content {
          display: none;
          position: absolute;
          background-color: #f1f1f1;
          min-width: 160px;
          font-size: 14px;
          box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
          z-index: 1;
        }

        .dropdown-content a {
          color: black;
          padding: 12px 16px;
          text-decoration: none;
          display: block;
        }

        .dropdown-content a:hover {
          background-color: #ddd;
        }

        .dropdown:hover .dropdown-content {
          display: block;
        }

        .dropdown:hover .dropbtn {
          background-color: #3e8e41;
        }
      </style>
  </head>
  <body>

    <div class="twp-video-background">
        <div class="twp-video-foreground">
          <video autoplay muted loop id="myVideo">
            <source src="{{ asset('img/video.mp4') }}" type="video/mp4" />
          </video>
        </div>
        <div class="twp-video-layer"></div>
      </div>

      <div class="dropdown">
        <button class="dropbtn">Dropdown</button>
        <div class="dropdown-content">
          <a href="#">My profile</a>
          <a href="#">Change password</a>
          <a href="#">Logout</a>
        </div>
      </div>
      <section class="home" id="home">
        <div class="container">


          <div class="quote-box">
            <p id="quote">
              <i
                >"To Live A Pure Unselfish Life, One Must Count Nothing As One’s
                Own In The Midst Of Abundance."</i
              >
            </p>
            <small id="author">- Buddha</small>
          </div>
          <button id="btn" style="background-color: transparent">Next</button>
        </div>
        <script>
          const api = "https://api.quotable.io/random";

          const quote = document.getElementById("quote");
          const author = document.getElementById("author");
          const btn = document.getElementById("btn");

          btn.addEventListener("click", getQuote);

          function getQuote() {
            fetch(api)
              .then((res) => res.json())
              .then((data) => {
                quote.innerHTML = `"${data.content}"`;
                author.innerHTML = `- ${data.author}`;
              });
          }
        </script>
      </section>

    <!-- header section starts  -->

    <header>
      <div class="user">
        <img src="{{ asset('img/logo.png') }}" alt="" />
        <h3 class="name">Erudite</h3>
        <p class="post">The Unified Platform</p>
      </div>

      <nav class="navbar">
        <ul>
          <li><a href="#home">home</a></li>
          <li><a href="#about">Quiz</a></li>
          <li><a href="#education">Assignment</a></li>

            @if (auth()->user()->type == "Teacher")
            <li><a href="{{ route('questions.index') }}">Answer question</a></li>
            @else
                <li><a href="{{ route('questions.index') }}">Ask a question</a></li>
            @endif


          {{-- <li><a href="{{ route('logout') }}">Trends</a></li> --}}
          <li>

            <form method="POST" action="{{ route('logout') }}">
                @csrf

                <x-dropdown-link :href="route('logout')"
                        onclick="event.preventDefault();
                                    this.closest('form').submit();">
                    {{ __('Log Out') }}
                </x-dropdown-link>
            </form>

          </li>
        </ul>
      </nav>
    </header>

    <!-- header section ends -->

    <div id="menu" class="fas fa-bars"></div>

    <!-- home section starts  -->

    <section class="home" id="home">
      <h3>API</h3>

      <p>Quotes</p>
    </section>

    <!-- home section ends -->

    <!-- about section starts  -->

    <section class="about" id="about">
      <h1 class="heading"><span>about</span> me</h1>

      <div class="row">
        <div class="info">
          <h3><span> name : </span> shaikh anas</h3>
          <h3><span> age : </span> 20</h3>
          <h3><span> qualification : </span> BMS</h3>
          <h3><span> post : </span> front end developer</h3>
          <h3><span> language : </span> hindi</h3>
          <a href="#"
            ><button class="btn">
              download CV <i class="fas fa-download"></i></button
          ></a>
        </div>

        <div class="counter">
          <div class="box">
            <span>2+</span>
            <h3>years of experience</h3>
          </div>

          <div class="box">
            <span>100+</span>
            <h3>porject completed</h3>
          </div>

          <div class="box">
            <span>430+</span>
            <h3>happy clients</h3>
          </div>

          <div class="box">
            <span>12+</span>
            <h3>awards won</h3>
          </div>
        </div>
      </div>
    </section>

    <!-- about section ends -->

    <!-- education section starts  -->

    <section class="education" id="education">
      <h1 class="heading">my <span>education</span></h1>

      <div class="box-container">
        <div class="box">
          <i class="fas fa-graduation-cap"></i>
          <span>2016</span>
          <h3>front end development</h3>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Obcaecati
            quos alias praesentium. Id autem provident laborum quae, distinctio
            eaque temporibus!
          </p>
        </div>

        <div class="box">
          <i class="fas fa-graduation-cap"></i>
          <span>2017</span>
          <h3>front end development</h3>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Obcaecati
            quos alias praesentium. Id autem provident laborum quae, distinctio
            eaque temporibus!
          </p>
        </div>

        <div class="box">
          <i class="fas fa-graduation-cap"></i>
          <span>2018</span>
          <h3>front end development</h3>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Obcaecati
            quos alias praesentium. Id autem provident laborum quae, distinctio
            eaque temporibus!
          </p>
        </div>

        <div class="box">
          <i class="fas fa-graduation-cap"></i>
          <span>2019</span>
          <h3>front end development</h3>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Obcaecati
            quos alias praesentium. Id autem provident laborum quae, distinctio
            eaque temporibus!
          </p>
        </div>

        <div class="box">
          <i class="fas fa-graduation-cap"></i>
          <span>2020</span>
          <h3>front end development</h3>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Obcaecati
            quos alias praesentium. Id autem provident laborum quae, distinctio
            eaque temporibus!
          </p>
        </div>

        <div class="box">
          <i class="fas fa-graduation-cap"></i>
          <span>2021</span>
          <h3>front end development</h3>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Obcaecati
            quos alias praesentium. Id autem provident laborum quae, distinctio
            eaque temporibus!
          </p>
        </div>
      </div>
    </section>

    <!-- education section ends -->

    <!-- portfolio section starts  -->

    <section class="portfolio" id="portfolio">
      <h1 class="heading">my <span>portfolio</span></h1>

      <div class="box-container">
        <div class="box">
          <img src="images/img1.jpg" alt="" />
        </div>

        <div class="box">
          <img src="images/img2.jpg" alt="" />
        </div>

        <div class="box">
          <img src="images/img3.jpg" alt="" />
        </div>

        <div class="box">
          <img src="images/img4.jpg" alt="" />
        </div>

        <div class="box">
          <img src="images/img5.jpg" alt="" />
        </div>

        <div class="box">
          <img src="images/img6.jpg" alt="" />
        </div>
      </div>
    </section>

    <!-- portfolio section ends -->

    <!-- contact section starts  -->

    <section class="contact" id="contact">
      <h1 class="heading"><span>contact</span> me</h1>

      <div class="row">
        <div class="content">
          <h3 class="title">contact info</h3>

          <div class="info">
            <h3><i class="fas fa-envelope"></i> shaikh@gmail.com</h3>
            <h3><i class="fas fa-phone"></i> +123-456-7890</h3>
            <h3><i class="fas fa-phone"></i> +111-222-3333</h3>
            <h3>
              <i class="fas fa-map-marker-alt"></i> mumbai, india - 400104.
            </h3>
          </div>
        </div>

        <form action="">
          <input type="text" placeholder="name" class="box" />
          <input type="email" placeholder="email" class="box" />
          <input type="text" placeholder="project" class="box" />
          <textarea
            name=""
            id=""
            cols="30"
            rows="10"
            class="box message"
            placeholder="message"
          ></textarea>
          <button type="submit" class="btn">
            send <i class="fas fa-paper-plane"></i>
          </button>
        </form>
      </div>
    </section>

    <!-- contact section ends -->

    <!-- scroll top button  -->

    <a href="#home" class="top">
      <img src="{{ asset('img/') }}" alt="" />
    </a>

    <!-- jquery cdn link  -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

    <!-- custom js file link  -->
    <script src="{{ asset('js/dashboard_blade.js') }}"></script>
  </body>
</html>


