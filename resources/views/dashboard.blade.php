<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Erudite</title>

    <!-- font awesome cdn link  -->
    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css"
    />

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <!-- custom css file link  -->
    <link rel="stylesheet" href="{{ asset('css/dashboard_blade.css') }}" />
    <style>
      .dropbtn {
        background-color: #04aa6d;
        color: white;
        padding: 16px;
        font-size: 16px;
        border: none;
      }

      .dropdown {
        position: relative;
        left: 85rem;
        top: 25px;
        display: inline-block;
      }

      .dropdown-content {
        display: none;
        position: absolute;
        background-color: #f1f1f1;
        min-width: 160px;
        font-size: 14px;
        box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
        z-index: 1;
      }

      .dropdown-content a, .dropdown-content button{
        color: black;
        padding: 12px 16px;
        text-decoration: none;
        display: block;
        width: 100%;
        text-align: left;
      }

      .dropdown-content a:hover ,.dropdown-content button:hover{
        background-color: #ddd;
      }

      .dropdown:hover .dropdown-content {
        display: block;
      }

      .dropdown:hover .dropbtn {
        background-color: #3e8e41;
      }

      .red-dot{
        width: max-content;
        padding: 0 0.6rem;
        height: 2.5rem;
        position: absolute;
        border-radius:1rem;
        top:-1rem;
        left: -1rem;
        background: red;
      }
    </style>
  </head>
  <body>
    <div class="twp-video-background">
      <div class="twp-video-foreground">
        <video autoplay muted loop id="myVideo">
          <source src="{{ asset('img/video.mp4') }}" type="video/mp4" />
        </video>
      </div>
      <div class="twp-video-layer"></div>
    </div>



    <div class="dropdown">
      <button class="dropbtn">{{ auth()->user()->name }}
        @if (auth()->user()->unreadNotifications()->count())
          <span class="red-dot">{{auth()->user()->unreadNotifications()->count()}}</span>

        @elseif (auth()->user()->isStudent())
            @if (!auth()->user()->image1 || !auth()->user()->image2)
            <span class="red-dot">1</span>
            @endif
        @endif
      </button>
      <div class="dropdown-content">
        {{-- <a href="#">My profile</a>
        <a href="#">Change password</a> --}}
        <form action="{{ route('logout') }}" method="POST" width="160px">
            @csrf
            <button type="submit">Logout</button>
        </form>

        @auth
        <a href="{{ route('notifications') }}" class="nav-link text-primary">
        {{ auth()->user()->unreadNotifications()->count() }} Notifications
         </a>
        @endauth

        @if (auth()->user()->isStudent() && (!auth()->user()->image1 || !auth()->user()->image2))
            <a class="text-danger" href="{{ route('userPhoto') }}">Upload Photo</a>
        @else
            <a href="">Uploaded</a>
        @endif

      </div>
    </div>
    <section class="home" id="home">
      <div class="container">


        <div class="quote-box">
          <p id="quote">
            <i
              >"To Live A Pure Unselfish Life, One Must Count Nothing As One’s
              Own In The Midst Of Abundance."</i
            >
          </p>
          <small id="author">- Buddha</small>
        </div>
        <button id="btn" style="background-color: transparent">Next</button>
      </div>
      <script>
        const api = "https://api.quotable.io/random";

        const quote = document.getElementById("quote");
        const author = document.getElementById("author");
        const btn = document.getElementById("btn");

        btn.addEventListener("click", getQuote);

        function getQuote() {
          fetch(api)
            .then((res) => res.json())
            .then((data) => {
              quote.innerHTML = `"${data.content}"`;
              author.innerHTML = `- ${data.author}`;
            });
        }
      </script>
    </section>

    <!-- header section starts  -->

    <header>
      <div class="user">
        <img src="{{ asset('img/logo.png') }}" alt="" />
        <!--
          <h3 class="name">Erudite</h3>
        <p class="post">The Unified Platform</p>-->
      </div>

      <nav class="navbar">
        <ul>
          <li><a href="#home">home</a></li>
          @if (auth()->user()->type == "Teacher")
                <li><a href="{{ route('quiz.teacher.home') }}">Quiz</a></li>
            @else
                <li><a href="" data-toggle="modal" data-target="#studentQuizmodal">Quiz</a></li>
            @endif

          @if (auth()->user()->type == "Teacher")
                <li><a href="{{ route('assignment.home') }}">Assignment</a></li>
            @else
                <li><a href="{{ route('assignment.homestudent') }}">Assignment</a></li>
            @endif
          @if (auth()->user()->type == "Teacher")
            <li><a href="{{ route('questions.index') }}">Answer question</a></li>
            @else
                <li><a href="{{ route('questions.index') }}">Ask a question</a></li>
            @endif
          {{-- <li><a href="#contact">Trivia Quiz</a></li> --}}
        </ul>
      </nav>
    </header>

    <!-- header section ends -->

    <div id="menu" class="fas fa-bars"></div>

    <!-- home section starts  -->

    <!-- home section ends -->

    <!-- about section starts  -->

    {{-- <section class="about" id="about">
      <h1 class="heading"><span>about</span> me</h1> --}}


    </section>

    <!-- about section ends -->

    <!-- education section starts  -->

    {{-- <section class="education" id="education"> --}}




      <!--
      <div class="box-container">
        <div class="box">
          <i class="fas fa-graduation-cap"></i>
          <span>2016</span>
          <h3>front end development</h3>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Obcaecati
            quos alias praesentium. Id autem provident laborum quae, distinctio
            eaque temporibus!
          </p>
        </div>

        <div class="box">
          <i class="fas fa-graduation-cap"></i>
          <span>2017</span>
          <h3>front end development</h3>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Obcaecati
            quos alias praesentium. Id autem provident laborum quae, distinctio
            eaque temporibus!
          </p>
        </div>

        <div class="box">
          <i class="fas fa-graduation-cap"></i>
          <span>2018</span>
          <h3>front end development</h3>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Obcaecati
            quos alias praesentium. Id autem provident laborum quae, distinctio
            eaque temporibus!
          </p>
        </div>

        <div class="box">
          <i class="fas fa-graduation-cap"></i>
          <span>2019</span>
          <h3>front end development</h3>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Obcaecati
            quos alias praesentium. Id autem provident laborum quae, distinctio
            eaque temporibus!
          </p>
        </div>

        <div class="box">
          <i class="fas fa-graduation-cap"></i>
          <span>2020</span>
          <h3>front end development</h3>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Obcaecati
            quos alias praesentium. Id autem provident laborum quae, distinctio
            eaque temporibus!
          </p>
        </div>

        <div class="box">
          <i class="fas fa-graduation-cap"></i>
          <span>2021</span>
          <h3>front end development</h3>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Obcaecati
            quos alias praesentium. Id autem provident laborum quae, distinctio
            eaque temporibus!
          </p>
        </div>
      </div>
    </section>
-->
    <!-- education section ends -->

    <!-- portfolio section starts  -->

    {{-- <section class="portfolio" id="portfolio">
      <h1 class="heading">my <span>portfolio</span></h1>


    </section> --}}

    <!-- portfolio section ends -->

    <!-- contact section starts  -->
    {{-- <section class="contact" id="contact">
      <h1 class="heading"><span>Play</span> Quiz</h1>

      <div class="row">
        <div class="content">
          <h3 class="title">A Quick Trivia Quiz</h3> --}}

          {{-- <div class="info">
            <a class="btn" href="game.html">Play</a>
          </div> --}}
        {{-- </div>
      </section> --}}

    <!-- contact section ends -->

    <!-- scroll top button  -->

    <!-- <a href="#home" class="top">
      <img src="images/scroll-top-img.png" alt="" />
    </a> -->
    {{-- <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Give Question ID</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                @if ($errors)
                    <ul>
                        @foreach ($errors->all() as $message)
                            <li>{{ $message }}</li>
                        @endforeach
                    </ul>
                @endif
                <form action="{{ route('students.assignment') }}" method="POST">
                    @csrf
                    <input type="text" name="assignment_id" class="" placeholder="Assignment ID" value="{{ request('assignment_id') }}">
                    <select name="users" id="teachers" class="form-control select2" multiple>
                        <option></option>
                        @foreach ($teachers as $teacher)
                           <option value="{{ $teacher->id }}">{{$teacher->name}}</option>

                        @endforeach
                    </select>
                    <button type="submit"class="btn btn-success"><i class="fa fa-submit"></i></button>
                </form>
            </div>
          </div>
        </div>
      </div> --}}

    <!-- Student Quiz Modal -->
    <div class="modal fade" id="studentQuizmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Quiz</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">

            <form action="{{ route('quiz.student') }}" method="POST">
                @if ($errors)
                        <ul>
                            @foreach ($errors->all() as $message)
                                <li>{{ $message }}</li>
                            @endforeach
                        </ul>
                    @endif
                @csrf
                <label for="quizNumber">Enter Quiz ID</label>
                <input type="text" name="quizCode">
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
        </div>
    </div>

    <!-- jquery cdn link  -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <!-- custom js file link  -->
    <script src="{{ asset('js/dashboard_blade.js') }}"></script>
    <script>
        $('.select2').select2({
        placeholder: 'Select a Teacher',
        allowClear: true,
        maximumSelectionLength: 1
    });
    </script>
  </body>
</html>
