@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="d-flex mb-3 justify-content-end">
                <div class="">
                    <form action="{{ route('admin.index') }}" method="GET">
                        <input type="text" name="search" class="" placeholder="Search Title" value="{{ request('search') }}">
                        <button type="submit"class="btn btn-success"><i class="fa fa-search"></i></button>
                    </form>
                </div>
            </div>
            <div class="card">
                <div class="card-header">All Users</div>
                @foreach ($users as $user)
                    <div class="card-body">
                        <div class="media">
                            <div class="media-body">
                                <div class="d-flex justify-content-between">
                                    <h4><a href="{{ route('admin.show', $user) }}">{{ $user->name }}</a></h4>
                                    <h6 class="float-right">User ID: {{ $user->id }}</h6>
                                <div>

                                            @if($user->isApproved())

                                                <a href="" class="btn mb-1 btn-success">Approved</a>

                                            @else

                                                <a href="" class="btn mb-1 btn-danger">Not Approved</a>

                                            @endif



                                            {{-- <form action="" method="POST">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete it?')">Delete</button>
                                            </form> --}}

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                @endforeach

                <div class="card-footer">
                    {{ $users->appends(['search' => request('search')])->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
