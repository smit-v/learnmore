@extends('layouts.app')
@section('content')
<div class="container">
    <a class="mb-3 btn btn-primary" href="{{ route('admin.index') }}">
        Back
    </a>

    @if(!$user->isApproved())
        <form action="{{ route('admin.approve', $user) }}" method="POST" class="inline">
            @csrf
            <button class="ml-3 mb-3 btn btn-warning" type="submit">
                Appove User
            </button>
        </form>
    @else
        <button class="ml-3 mb-3 btn btn-success inline">
            Appoved
        </button>
    @endif
    <div class="row justify-content-center">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header">
                    <h1 class="pull-left">{{ $user->name }}</h1>
                </div>
                <div class="card-body">
                    <h3 class="">ID:{{ $user->id }}</h3>
                    @if ($errors)
                    <ul>
                        @foreach ($errors->all() as $message)
                            <li class="text-danger">{{ $message }}</li>
                        @endforeach
                    </ul>
                    @endif

                    <form action="" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" name="name" id=name value="{{ old('name', $user->name) }}" required>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" name="email" id=email value="{{ old('email', $user->email) }}" required>
                                <input type="hidden" name="id" value="{{ $user->id }}">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="organization">Organization</label>
                                <input id="organization" type="text" class="form-control" name="organization" value="{{ old('organization', $user->organization) }}"required>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="type">Type</label>
                                <input id="type" type="text" class="form-control" name="type" value="{{ old('type', $user->type) }}"required>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-warning">Edit</button>
                    </form>
                </div>
                <div class="card-footer">
                    <div class="d-flex justify-content-between">
                        <div class="d-flex">

                                    @if($user->image1)
                                        <a href="{{ asset($user->image_path1) }}" class="btn btn-primary mr-2">View Image 1</a>
                                    @else
                                        <a href="" class="btn btn-primary mr-2">No Image 1</a>
                                    @endif
                                    @if($user->image2)
                                        <a href="{{ asset($user->image_path2) }}" class="btn btn-primary mr-4">View Image 2</a>
                                    @else
                                        <a href="" class="btn btn-primary mr-4">No Image 2</a>
                                    @endif
                                    @if($user->image1)
                                        <form action="{{ route('admin.delete.Image', $user) }}" method="POST">
                                            @csrf
                                            <button type="submit" class="btn btn-danger mr-2" onclick="return confirm('Are you sure you want to delete it?')">Delete Images</a>
                                        </form>

                                    @else

                                    @endif



                            <form action="{{ route('admin.delete', $user) }}" method="POST" class="mr-3 pull-right">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete it?')">Delete User</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
