@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><h1>Notifications</h1></div>
                <div class="card-body">
                    <ul class="list-group">
                        @foreach ($notifications as $notification)
                            <li class="list-group-item">
                                @if($notification->type == App\Notifications\NewQuestionAsked::class)
                                    Question asked is:  <strong>{{ $notification->data['question']['title'] }}</strong>
                                    <a href="{{ route('questions.show',$notification->data['question']['slug']) }}" class="btn btn-sm btn-info text-white float right">
                                    View Question
                                    </a>
                                @endif

                                @if($notification->type == App\Notifications\NewAnswerAdded::class)
                                    A new Answer is added to:  <strong>{{ $notification->data['question']['title'] }}</strong>
                                    <a href="{{ route('questions.show',$notification->data['question']['slug']) }}" class="btn btn-sm btn-info text-white float right">
                                    View Question
                                    </a>
                                @endif

                                @if($notification->type == App\Notifications\AssignmentReturned::class)
                                    Submitted Assignment returned:  <strong>{{ $notification->data['assignment']['title'] }}</strong>
                                    <a href="{{ route('students.assignment.show',$notification->data['assignment']['id']) }}" class="btn btn-sm btn-info text-white float right">
                                    View Assignment
                                    </a>
                                @endif

                                @if($notification->type == App\Notifications\AssignmentChanged::class)
                                    Assignment edited:  <strong>{{ $notification->data['assignment']['title'] }}</strong>
                                    <a href="{{ route('students.assignment.show',$notification->data['assignment']['id']) }}" class="btn btn-sm btn-info text-white float right">
                                    View Assignment
                                    </a>
                                @endif

                                @if($notification->type == App\Notifications\AssignmentReminder::class)
                                    Assignment Reminder:  <strong>{{ $notification->data['assignment']['title'] }}</strong>
                                    <a href="{{ route('students.assignment.show',$notification->data['assignment']['id']) }}" class="btn btn-sm btn-info text-white float right">
                                    View Assignment
                                    </a>
                                @endif

                                @if($notification->type == App\Notifications\StudentReexam::class)
                                    {{-- {{ dd($notification->data) }} --}}
                                    Re attempt Quiz:  {{ $notification->data['quiz'] }}
                                    Code: <strong>{{ $notification->data['quizcode'] }}</strong>
                                    Your Marks are NULL
                                @endif
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
