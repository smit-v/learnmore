{{-- <x-guest-layout>
    <x-auth-card>
        <x-slot name="logo">
            <a href="/">
                <x-application-logo class="w-20 h-20 fill-current text-gray-500" />
            </a>
        </x-slot>

        <!-- Session Status -->
        <x-auth-session-status class="mb-4" :status="session('status')" />

        <!-- Validation Errors -->
        <x-auth-validation-errors class="mb-4" :errors="$errors" />

        <form method="POST" action="{{ route('login') }}">
            @csrf

            <!-- Email Address -->
            <div>
                <x-label for="email" :value="__('Email')" />

                <x-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required autofocus />
            </div>

            <!-- Password -->
            <div class="mt-4">
                <x-label for="password" :value="__('Password')" />

                <x-input id="password" class="block mt-1 w-full"
                                type="password"
                                name="password"
                                required autocomplete="current-password" />
            </div>

            <!-- Remember Me -->
            <div class="block mt-4">
                <label for="remember_me" class="inline-flex items-center">
                    <input id="remember_me" type="checkbox" class="rounded border-gray-300 text-indigo-600 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50" name="remember">
                    <span class="ml-2 text-sm text-gray-600">{{ __('Remember me') }}</span>
                </label>
            </div>

            <div class="flex items-center justify-end mt-4">
                @if (Route::has('password.request'))
                    <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('password.request') }}">
                        {{ __('Forgot your password?') }}
                    </a>
                @endif

                <x-button class="ml-3">
                    {{ __('Log in') }}
                </x-button>
            </div>
        </form>
    </x-auth-card>
</x-guest-layout> --}}


<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- ===== CSS ===== -->
        <link rel="stylesheet" href="{{ asset('css/our_styles.css') }}">

        <!-- ===== BOX ICONS ===== -->
        <link href='https://cdn.jsdelivr.net/npm/boxicons@2.0.5/css/boxicons.min.css' rel='stylesheet'>

        <title>Ask and Learn Login</title>
        <style>
            img{

            }
        </style>
    </head>
    <body>
        <div class="login login_my">
            <div class="login__content">
                <div class="login__img">
                    <img src="{{ asset('img/logo-1.svg') }}" alt="">
                </div>

                <div class="login__forms">
                    <form method="POST" action="{{ route('login') }}" class="login__registre" id="login-in">
                        @csrf
                        <h1 class="login__title">Sign In</h1>

                        @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

                        <div class="login__box">
                            <i class='bx bx-user login__icon'></i>

                            <x-input id="email" class="block mt-1 w-full login__input" placeholder="Email ID" type="email" name="email" :value="old('email')" required autofocus />
                        </div>

                        <div class="login__box">
                            <i class='bx bx-lock-alt login__icon'></i>
                            {{-- <input type="password" placeholder="Password" class="login__input"> --}}
                            <x-input id="password" class="block mt-1 w-full"
                                type="password"
                                name="password"
                                placeholder="Password"
                                class="login__input"
                                required autocomplete="current-password" />
                        </div>

                        <a href="{{ route('password.request') }}" class="login__forgot">Forgot password?</a>
                        <a href="{{ route('home') }}" class="login__forgot">Back to home page</a>

                        {{-- <a href="#" class="login__button">Sign In</a> --}}
                        <x-button class="login__button">
                            {{ __('Log in') }}
                        </x-button>

                        <div>
                            <span class="login__account">Don't have an Account ?</span>
                            <span class="login__signin" id="sign-up"><a href="{{ route('register') }}">Sign Up</a></span>
                        </div>
                    </form>


                </div>
            </div>
        </div>

        <!--===== MAIN JS =====-->
        <script src="{{ asset("js/our_main.js") }}"></script>
    </body>
</html>
