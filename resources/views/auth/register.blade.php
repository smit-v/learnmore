{{-- <x-guest-layout>
    <x-auth-card>
        <x-slot name="logo">
            <a href="/">
                <x-application-logo class="w-20 h-20 fill-current text-gray-500" />
            </a>
        </x-slot>

        <!-- Validation Errors -->
        <x-auth-validation-errors class="mb-4" :errors="$errors" />

        <form method="POST" action="{{ route('register') }}">
            @csrf

            <!-- Name -->
            <div>
                <x-label for="name" :value="__('Name')" />

                <x-input id="name" class="block mt-1 w-full" type="text" name="name" :value="old('name')" required autofocus />
            </div>

            <!-- Email Address -->
            <div class="mt-4">
                <x-label for="email" :value="__('Email')" />

                <x-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required />
            </div>

            <!-- organization Address -->
            <div class="mt-4">
                <x-label for="password" :value="__('Organization')" />
                <x-input id="email" class="block mt-1 w-full" type="text" name="organization" :value="old('organization')" required />
            </div>

            <!-- Password -->
            <div class="mt-4">
                <x-label for="password" :value="__('Password')" />

                <x-input id="password" class="block mt-1 w-full"
                                type="password"
                                name="password"
                                required autocomplete="new-password" />
            </div>

            <!-- Confirm Password -->
            <div class="mt-4">
                <x-label for="password_confirmation" :value="__('Confirm Password')" />

                <x-input id="password_confirmation" class="block mt-1 w-full"
                                type="password"
                                name="password_confirmation" required />
            </div>

            <div class="mt-4">
                <x-label for="type" :value="__('Teacher')" />
                <input type="radio" id="a" name="type" value="Teacher" />
                <x-label for="type" :value="__('Student')" />
                <input type="radio" id="b" name="type" value="Student" />
            </div>


            <div class="flex items-center justify-end mt-4">
                <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('login') }}">
                    {{ __('Already registered?') }}
                </a>

                <x-button class="ml-4">
                    {{ __('Register') }}
                </x-button>
            </div>
        </form>
    </x-auth-card>
</x-guest-layout> --}}


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <!-- ===== CSS ===== -->
    <link rel="stylesheet" href="{{ asset('css/our_styles.css') }}" />


    <!-- ===== BOX ICONS ===== -->
    <link
      href="https://cdn.jsdelivr.net/npm/boxicons@2.0.5/css/boxicons.min.css"
      rel="stylesheet"
    />

    <title>Ask and Learn Login</title>
  </head>
  <body>
    <div class="login">
      <div class="login__content">
        <div class="login__img">
            <img src="{{ asset('img/logo-1.svg') }}" alt="">
        </div>

        <div class="login__forms">
          <form method="POST" action="{{ route('register') }}" class="login__registre" id="login-in">
            @csrf
            <h1 class="login__title">Sign Up</h1>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="login__box">
              <i class="bx bx-user login__icon"></i>
              <x-input id="name" class="login__input" type="text" placeholder="Name" name="name" :value="old('name')" required autofocus />
            </div>
            <div class="login__box">
              <i class="bx bx-user login__icon"></i>
              {{-- <input
                name="organization"
                type="text"
                placeholder="Organisation"
                class="login__input"
                required
              /> --}}
              <x-input id="organization" class="login__input" type="text" placeholder="Orgainsiation" name="organization" :value="old('organization')" required autofocus />
            </div>
            <div class="login__box">
              <i class="bx bx-user login__icon"></i>
              {{-- <input type="email" placeholder="Email" class="login__input" name="email" required/> --}}
              <x-input id="email" class="login__input" placeholder="Email ID" type="email" name="email" :value="old('email')" required />
            </div>

            <div class="login__box">
              <i class="bx bx-lock-alt login__icon"></i>
              {{-- <input
                type="password"
                placeholder="Password"
                class="login__input"
                name="password"
                required
              /> --}}
              <x-input id="password" class="login__input"
                                type="password"
                                name="password"
                                placeholder="Password"
                                required autocomplete="new-password" />
            </div>

            <div class="login__box">
                <i class="bx bx-lock-alt login__icon"></i>
                {{-- <input
                  type="password"
                  placeholder="Confirm Password"
                  class="login__input"
                  name="password-confirmation"
                  required
                /> --}}
                <x-input id="password_confirmation" class="login__input"
                                type="password"
                                placeholder="Confirm Password"
                                name="password_confirmation" required />
              </div>

            <div class="login__box">
              <label for="a">Teacher</label>
              <input type="radio" id="a" name="type" value="Teacher" />
              <label for="b">Student</label>
              <input type="radio" id="b" name="type" value="Student" />
            </div>
            {{-- <input type="submit" class="login__button" value="Sign up"/> --}}
            <a href="{{ route('home') }}" class="login__forgot">Back to home page</a>
            <x-button class="login__button">
                {{ __('Register') }}
            </x-button>
            <div>
              <span class="login__account">Already have an account?</span>
              <span class="login__signin" id="sign-in"><a href="{{ route('login') }}">Log In</a></span>
            </div>
          </form>
        </div>
      </div>
    </div>

    <!--===== MAIN JS =====-->
    <script src="{{ asset('js/our_main.js') }}"></script>

  </body>
</html>
