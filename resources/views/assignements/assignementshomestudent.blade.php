<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Erudite</title>

    <!-- font awesome cdn link  -->

    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css"
    />
    <link rel="preconnect" href="https://fonts.gstatic.com">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <!-- custom css file link  -->
    <link rel="stylesheet" href="{{ asset('css/dashboard_blade.css') }}" />
    <style>
      .dropbtn {
        background-color: #04aa6d;
        color: white;
        padding: 16px;
        font-size: 16px;
        border: none;
      }

      .dropdown {
        position: relative;
        left: 87.696969%;
        top: 25px;
        display: inline-block;
      }

      .dropdown-content {
        display: none;
        position: absolute;
        background-color: #f1f1f1;
        min-width: 160px;
        font-size: 14px;
        box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
        z-index: 1;
      }

      .dropdown-content a {
        color: black;
        padding: 12px 16px;
        text-decoration: none;
        display: block;
      }

      .dropdown-content a:hover {
        background-color: #ddd;
      }

      .dropdown:hover .dropdown-content {
        display: block;
      }

      .dropdown:hover .dropbtn {
        background-color: #3e8e41;
      }
    </style>
  </head>
  <body>
    {{-- <div class="dropdown">
      <button class="dropbtn">Dropdown</button>
      <div class="dropdown-content">
        <a href="#">My profile</a>
        <a href="#">Change password</a>
        <a href="#">Link 3</a>
      </div>
    </div> --}}
    <!-- header section starts  -->

    <header>
      <div class="user">
        <img src="{{ asset('img/logo.png') }}" alt="" />
        <!--
          <h3 class="name">Erudite</h3>
        <p class="post">The Unified Platform</p>-->
      </div>

      <nav class="navbar">
        <ul>
          <li><a href="{{ route('dashboard') }}">home</a></li>
          <li><a data-toggle="modal" data-target="#exampleModal">Join Assignment</a></li>
          <li><a href="{{ route('students.assignment.index') }}">Manage Assignment</a></li>
        </ul>
      </nav>
    </header>

    <!-- header section ends -->

    <div id="menu" class="fas fa-bars"></div>

    <!-- home section starts  -->

    <!-- home section ends -->



    <!-- about section ends -->

    <!-- education section starts  -->

    <section class="education" id="education">

</section>
    <!-- education section ends -->

    <!-- portfolio section starts  -->

    <section class="portfolio" id="portfolio">
      <div class="containe">
      <div class="text"><h2>Student List</h2></div>
      </div>
      <table>
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Due Date</th>
            <th>Status</th>
            <th>Grade</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td><a href="#">101</a></td>
            <td>Smit V</td>
            <td>1/5/2021</td>
            <td>
              <p class="status status-unpaid">Past due</p>
            </td>
            <td class="amount">5</td>
          </tr>
          <tr>
            <td><a href="#">102</a></td>
            <td>Smit Vag</td>
            <td>1/4/2021</td>
            <td>
              <p class="status status-paid">Completed</p>
            </td>
            <td class="amount">4</td>
          </tr>
          <tr>
            <td><a href="#">103</a></td>
            <td>Smit Vagh</td>
            <td>1/2/2021</td>
            <td>
              <p class="status status-pending">Assigned</p>
            </td>
            <td class="amount">3</td>
          </tr>
          <tr>
            <td><a href="#">104</a></td>
            <td>Smit Vaghe</td>
            <td>12/30/2020</td>
            <td>
              <p class="status status-pending">Assigned</p>
            </td>
            <td class="amount">9</td>
          </tr>
          <tr>
            <td><a href="#">105</a></td>
            <td>Smit Vaghela</td>
            <td>12/18/2020</td>
            <td>
              <p class="status status-paid">Completed</p>
            </td>
            <td class="amount">7</td>
          </tr>
        </tbody>
      </table>
      <!--
      <h1 class="heading">my <span>portfolio</span></h1>

      <div class="box-container">
        <div class="box">
          <img src="images/img1.jpg" alt="" />
        </div>

        <div class="box">
          <img src="images/img2.jpg" alt="" />
        </div>

        <div class="box">
          <img src="images/img3.jpg" alt="" />
        </div>

        <div class="box">
          <img src="images/img4.jpg" alt="" />
        </div>

        <div class="box">
          <img src="images/img5.jpg" alt="" />
        </div>

        <div class="box">
          <img src="images/img6.jpg" alt="" />
        </div>
      </div>-->
    </section>

    <!-- portfolio section ends -->

    <!-- contact section starts  -->
    <section class="contact" id="contact">
      <h1 class="heading"><span>Play</span> Quiz</h1>

      <div class="row">
        <div class="content">
          <h3 class="title">A Quick Trivia Quiz</h3>

          <div class="info">
            <a class="btn" href="game.html">Play</a>
          </div>
        </div>
      </section>


      <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Give Question ID</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                @if ($errors)
                    <ul>
                        @foreach ($errors->all() as $message)
                            <li>{{ $message }}</li>
                        @endforeach
                    </ul>
                @endif
                <form action="{{ route('students.assignment.accept') }}" method="POST">
                    @csrf
                    <input type="text" name="assignment_id" class="" placeholder="Assignment ID" value="{{ request('assignment_id') }}">
                    <select name="users" id="teachers" class="form-control select2" multiple>
                        <option></option>
                        @foreach ($teachers as $teacher)
                           <option value="{{ $teacher->id }}">{{$teacher->name}}</option>

                        @endforeach
                    </select>
                    <button type="submit"class="btn btn-success"><i class="fas fa-arrow-right"></i></button>
                </form>
            </div>
          </div>
        </div>
      </div>

    <!-- contact section ends -->

    <!-- scroll top button  -->

    <a href="#home" class="top">
      <img src="images/scroll-top-img.png" alt="" />
    </a>

    <!-- jquery cdn link  -->

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

    <!-- custom js file link  -->
    <script src="{{ asset('js/dashboard_blade.js') }}"></script>
    <script>
        $('.select2').select2({
        placeholder: 'Select a Teacher',
        allowClear: true,
        maximumSelectionLength: 1
    });
    </script>
  </body>
</html>
