<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Erudite</title>

    <!-- font awesome cdn link  -->
    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css"

    />

    <link rel="preconnect" href="https://fonts.gstatic.com">

    <!-- custom css file link  -->
    <link rel="stylesheet" href="{{ asset('css/dashboard_blade.css') }}" />
    <style>
        .about{
            margin-top: 10rem;
        }

        section.about{
            min-height: 80vh;
        }
    </style>
  </head>
  <body>

    <!-- header section starts  -->

    <header>
      <div class="user">
        <img src="{{ asset('img/logo.png') }}" alt="" />
        <!--
          <h3 class="name">Erudite</h3>
        <p class="post">The Unified Platform</p>-->
      </div>

      <nav class="navbar">
        <ul>
          <li><a href="{{ route('assignment.home') }}">home</a></li>
          <li><a href="{{ route('assignments.show', $assignment->id) }}">Show</a></li>
          {{-- <li><a href="#about">Create Assignment</a></li>
          <li><a href="#education">Manage Assignment</a></li>
          <li><a href="#portfolio">Assignment list</a></li>
          <li><a href="#contact">Trivia Quiz</a></li> --}}
        </ul>
      </nav>
    </header>

    <!-- header section ends -->

    <div id="menu" class="fas fa-bars"></div>

    <!-- home section starts  -->

    <!-- home section ends -->

    <!-- about section starts  -->
    <section class="about" id="about">
    <div class="containe">
        <div class="text"><h2>Assignment Update</h2></div>
        <form action="{{ route('assignments.update', $assignment->id) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
           <div class="form-row">
            @if ($errors)
                <ul>
                    @foreach ($errors->all() as $message)
                        <li>{{ $message }}</li>
                    @endforeach
                </ul>
            @endif
           </div>

           <div class="form-row">
            <div class="input-data">
                 <input type="text" name='title' value="{{ old('title', $assignment->title) }}" required>
                 <div class="underline"></div>
                 <label for="">Assignment name</label>
              </div>

              <div class="input-data">
                <input type="date" name="due_date" value="{{ old('due_date', $assignment->due_date) }}"required>
                <div class="underline"></div>
                <label for=""></label>
             </div>
           </div>

           <div class="form-row">
            <div class="input-data">
                <input type="number" name="total_marks" value="{{ old('total_marks', $assignment->total_marks) }}" required>
                <div class="underline"></div>
                <label for="">Points</label>
             </div>

                <div class="input-data">
                 <input type="time" name="due_time" value="{{ old('due_time', substr($assignment->due_time,0,5)) }}">
                 <p>Current time in DB: {{ $assignment->due_time }}</p>
                 <div class="underline"></div>
                 <label for=""></label>
                </div>
           </div>

           <div class="form-row">
            <div class="input-data">
                <input type="text" name="description" value="{{ old('description', $assignment->description) }}" required>
                <div class="underline"></div>
                <label for="">Reference Text</label>
               </div>
            </div>

            <div class="form-row">
                <div>
                    <p>Current file in Database: <a href="{{ asset($assignment->ref_path) }}">View file</a></p>
                </div>
                <div class="input-data">
                    <input type="file" name="ref">
                    <div class="underline"></div>
                    <label for=""></label>
                   </div>
            </div>

            <div class="form-row submit-btn">
            <div class="input-data">
                <div class="inner"></div>
                <input type="submit" value="submit">
            </div>
            </div>
        </form>
     </div>
    </section>


    <!-- contact section ends -->

    <!-- scroll top button  -->

    {{-- <a href="#home" class="top">
      <img src="images/scroll-top-img.png" alt="" />
    </a> --}}

    <!-- jquery cdn link  -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

    <!-- custom js file link  -->
    <script src="{{ asset('js/dashboard_blade.js') }}"></script>
  </body>
</html>
