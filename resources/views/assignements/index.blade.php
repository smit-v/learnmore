@extends('layouts.app')
@section('content')
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="d-flex mb-3 justify-content-end">
                <a class="mr-3 btn btn-primary" href="{{ route('assignment.home') }}">
                    Back to Dashboard
                </a>

                {{-- <div class="">
                    <form action="{{ route('assignments.index') }}" method="GET">
                        <input type="text" name="search" class="" placeholder="Search Assignment" value="{{ request('search') }}">
                        <button type="submit" class="btn btn-success"><i class="fa fa-search"></i></button>
                    </form>
                </div> --}}
                {{-- <button type="button" class="ml-3 btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                    Search by ID
                </button> --}}
            </div>
            <div class="card">
                <div class="card-header">All Assignments</div>
                @foreach ($assignments as $assignment)
                    <div class="card-body">
                        <div class="media">
                            <div class="media-body">
                                <div class="d-flex justify-content-between">
                                    <h4><a href="{{ route('assignments.show', $assignment->id) }}">{{ $assignment->title }}</a></h4>
                                    <h6 class="float-right">Assignment ID: {{ $assignment->id }}</h6>
                                <div>

                                        <a href="{{ route('assignments.edit',$assignment->id) }}" class="btn btn-warning">Edit</a>
                                        <form action="{{ route('assignments.destroy',$assignment->id) }}" method="POST" class="inline">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete it?')">Delete</button>
                                        </form>


                                        {{-- @can('delete',$question)
                                            <form action="{{ route('questions.destroy',$assi->id) }}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete it?')">Delete</button>
                                            </form>
                                        @endcan --}}
                                    </div>
                                </div>
                                <div class="text-muted mb-2 text-right">
                                    <p>Due Date - {{ $assignment->due_date }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                @endforeach

                <div class="card-footer">
                    {{-- {{ $questions->appends(['search' => request('search')])->links() }} --}}
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Give Question ID</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('questions.showById') }}" method="POST">
                    @csrf
                    <input type="text" name="searchById" class="" placeholder="Question ID" value="{{ request('searchById') }}">
                    <button type="submit"class="btn btn-success"><i class="fa fa-search"></i></button>
                </form>
            </div>
          </div>
        </div>
      </div>
</div>
@endsection
