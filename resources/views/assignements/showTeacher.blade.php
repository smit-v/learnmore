@extends('layouts.app')
@section('page-level-styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/trix/1.3.1/trix.css" integrity="sha512-CWdvnJD7uGtuypLLe5rLU3eUAkbzBR3Bm1SFPEaRfvXXI2v2H5Y0057EMTzNuGGRIznt8+128QIDQ8RqmHbAdg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<style>
    trix-toolbar .trix-button--icon-attach{
  display: none;
}
</style>
@endsection
@section('content')
<div class="container">
    <a class="mb-3 btn btn-primary" href="{{ route('assignments.show', $userassignment->assignment->id) }}">
        Back
    </a>

    <div class="row justify-content-center">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <h2 class="p-3">{{ $userassignment->assignedTo->name }}</h1>
                    </div>
                    {{-- <form class="pull-right" action="{{ route('students.assignment.withdraw', $userassignment->id) }}" method="POST">
                        @csrf
                        <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete it?')">Delete</button>
                    </form> --}}
                    <div class="">
                        @if(!$userassignment->submission_date)
                        <h3 class="pull-left text-danger">Student has not yet Submitted</h3>
                    @else
                        <h3 class="pull-left text-success">Submitted</h3>
                        <h5 class="pull-right">Submission date and time: {{ $userassignment->submission_date }} {{ $userassignment->submission_time }}</h5>
                    @endif
                    </div>
                </div>
                <div class="card-body">
                    {!! $userassignment->assignment->description !!}

                    <p>Ref Doc: <a href="{{ asset($userassignment->assignment->ref_path) }}">View file</a></p>

                    @if ($userassignment->submission_date)
                        <hr>

                        <p>Text from submission: {{ $userassignment->textOrfeedback }}</p>
                        <p>Submitted Doc: <a href="{{ asset($userassignment->doc_path) }}">View submission</a></p>
                        <p>Allotted Marks: {{ $userassignment->alloted_marks }}</p>
                    @endif

                </div>
                <div class="card-footer">
                    <div class="d-flex justify-content-between">
                        <div class="d-flex">

                        </div>
                        <div class="d-flex flex-column">
                            <div class="text-muted mb-2 text-right">
                                <p>Due Date : {{ $userassignment->assignment->due_date }}</p>
                            </div>
                            <div class="text-muted mb-2 text-right">
                                <p>Due Time : {{ $userassignment->assignment->due_time }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card mt-5">
                <div class="card-body">
                    @if ($errors)
                    <ul>
                        @foreach ($errors->all() as $message)
                            <li>{{ $message }}</li>
                        @endforeach
                    </ul>
                    @endif
                        @if ($userassignment->submission_date)
                        <form action="{{ route('teachers.assignment.update', $userassignment->id) }}" method="POST">
                            @csrf
                            @method('PUT')

                            @if (!$userassignment->alloted_marks)
                                <label for="alloted_marks">Grade the Assignment</label>
                                <input type="number" id="alloted_marks" name="alloted_marks" class="form-control">

                                <input type="submit" value="Grade" class="btn btn-primary mt-2">
                            @else

                                <h6>Allotted Marks: {{ $userassignment->alloted_marks }}</h6>
                                <label for="alloted_marks">Update grades</label>
                                <input type="number" id="alloted_marks" name="alloted_marks" class="form-control">

                                <input type="submit" value="Update Grade" class="btn btn-warning mt-2">
                            @endif

                        </form>
                        @endif
                </div>

            </div>

            {{-- <div class="card">
                <div class="card-header"><h1>{{ $students->name }}</h1></div>
                <div class="card-body">
                    {!! $assignment->description !!}
                </div>
                <div class="card-footer">
                    <div class="d-flex justify-content-between">
                        <div class="d-flex">

                        </div>
                        <div class="d-flex flex-column">
                            <div class="text-muted mb-2 text-right">
                                <p>Due Date - {{ $assignment->due_date }}</p>
                            </div>

                        </div>
                    </div>
                </div>
            </div> --}}
        </div>
    </div>

</div>
@endsection
@section('page-level-scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/trix/1.3.1/trix.js" integrity="sha512-/1nVu72YEESEbcmhE/EvjH/RxTg62EKvYWLG3NdeZibTCuEtW5M4z3aypcvsoZw03FAopi94y04GhuqRU9p+CQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
    function clearAll()
    {
        var output = document.getElementById("body1");
        output.value = "";
    }


    function runSpeechRecognition() {

		        var output = document.getElementById("body1");
		        // get action element reference
		        var action = document.getElementById("action");
                // new speech recognition object
                var SpeechRecognition = SpeechRecognition || webkitSpeechRecognition;
                var recognition = new SpeechRecognition();

                // This runs when the speech recognition service starts
                recognition.onstart = function() {
                    action.innerHTML = "<small>listening, please speak...</small>";
                };

                recognition.onspeechend = function() {
                    action.innerHTML = "<small>stopped listening, hope you are done...</small>";
                    recognition.stop();
                }

                // This runs when the speech recognition service returns result
                recognition.onresult = function(event) {
                    var transcript = event.results[0][0].transcript;
                    // console.log(transcript);
                    var original_val = output.value;
                    output.value = output.value + " " + transcript;
                    output.classList.remove("hide");
                };

                 // start recognition
                 recognition.start();
	        }
</script>
@endsection


