<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Erudite</title>

    <!-- font awesome cdn link  -->
    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css"

    />
    <link rel="preconnect" href="https://fonts.gstatic.com">


    <!-- custom css file link  -->
    <link rel="stylesheet" href="{{ asset('css/dashboard_blade.css') }}" />
    <style>
      .dropbtn {
        background-color: #04aa6d;
        color: white;
        padding: 16px;
        font-size: 16px;
        border: none;
      }

      .dropdown {
        position: relative;
        left: 87.696969%;
        top: 25px;
        display: inline-block;
      }

      .dropdown-content {
        display: none;
        position: absolute;
        background-color: #f1f1f1;
        min-width: 160px;
        font-size: 14px;
        box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
        z-index: 1;
      }

      .dropdown-content a {
        color: black;
        padding: 12px 16px;
        text-decoration: none;
        display: block;
      }

      .dropdown-content a:hover {
        background-color: #ddd;
      }

      .dropdown:hover .dropdown-content {
        display: block;
      }

      .dropdown:hover .dropbtn {
        background-color: #3e8e41;
      }
    </style>
  </head>
  <body>
    {{-- <div class="dropdown">
      <button class="dropbtn">Dropdown</button>
      <div class="dropdown-content">
        <a href="#">My profile</a>
        <a href="#">Change password</a>
        <a href="#">Link 3</a>
      </div>
    </div> --}}
    <!-- header section starts  -->

    <header>
      <div class="user">
        <img src="{{ asset('img/logo.png') }}" alt="" />
        <!--
          <h3 class="name">Erudite</h3>
        <p class="post">The Unified Platform</p>-->
      </div>

      <nav class="navbar">
        <ul>
          <li><a href="{{ route('dashboard') }}">home</a></li>
          <li><a href="{{ route('assignments.create') }}">Create Assignment</a></li>
          <li><a href="{{ route('assignments.index') }}">Manage Assignment</a></li>
          <li><a href="#portfolio">Assignment list</a></li>

        </ul>
      </nav>
    </header>

    <!-- header section ends -->

    <div id="menu" class="fas fa-bars"></div>

    <!-- home section starts  -->

    <!-- home section ends -->

    <!-- about section starts  -->

<!--
      <h1 class="heading"><span>about</span> me</h1>

      <div class="row">
        <div class="info">
          <h3><span> name : </span> shaikh anas</h3>
          <h3><span> age : </span> 20</h3>
          <h3><span> qualification : </span> BMS</h3>
          <h3><span> post : </span> front end developer</h3>
          <h3><span> language : </span> hindi</h3>
          <a href="#"
            ><button class="btn">
              download CV <i class="fas fa-download"></i></button
          ></a>
        </div>

        <div class="counter">
          <div class="box">
            <span>2+</span>
            <h3>years of experience</h3>
          </div>

          <div class="box">
            <span>100+</span>
            <h3>porject completed</h3>
          </div>

          <div class="box">
            <span>430+</span>
            <h3>happy clients</h3>
          </div>

          <div class="box">
            <span>12+</span>
            <h3>awards won</h3>
          </div>
        </div>
      </div>
    </section>
-->
    <!-- about section ends -->

    <!-- education section starts  -->

    <section class="education" id="education">



        {{-- <div class="containe">
            <div class="text"><h2>Edit Assignment</h2></div>
            <form action="#">
               <div class="form-row">
                  <div class="input-data">
                     <input type="text" required>
                     <div class="underline"></div>
                     <label for="">Assignment name</label>
                  </div>
                  <div class="input-data">
                     <input type="number" required>
                     <div class="underline"></div>
                     <label for="">Token</label>
                  </div>
               </div>
               <div class="form-row">
                  <div class="input-data">
                     <input type="date" required>
                     <div class="underline"></div>
                     <label for=""></label>
                  </div>
                  <div class="input-data">
                     <input type="number" required>
                     <div class="underline"></div>
                     <label for="">Points</label>
                  </div>
               </div>
               <div class="form-row">
                  <div class="input-data textarea">
                     <textarea rows="8" cols="80" required></textarea>
                     <br />
                     <div class="underline"></div>
                     <label for="">Write your message</label>
                     <br />

                     <div class="form-row submit-btn">
                        <div class="input-data">
                           <div class="inner"></div>
                           <input type="submit" value="submit">
                        </div>
                     </div>
                  </div>
               </div>
            </form>
         </div> --}}
         <!--
      <h1 class="heading">my <span>education</span></h1>



        <div class="menu__container bd-grid">
          <div class="menu__content">
            <img src="assets/img/1.png" alt="" class="menu__img" />
            <h3 class="menu__name">Doubt Solving</h3>
            <span class="menu__detail"
              >Students can get their doubts solved from teachers and other
              students all across the globe</span
            >





      <div class="box-container">
        <div class="box">
          <i class="fas fa-graduation-cap"></i>
          <span>2016</span>
          <h3>front end development</h3>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Obcaecati
            quos alias praesentium. Id autem provident laborum quae, distinctio
            eaque temporibus!
          </p>
        </div>

        <div class="box">
          <i class="fas fa-graduation-cap"></i>
          <span>2017</span>
          <h3>front end development</h3>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Obcaecati
            quos alias praesentium. Id autem provident laborum quae, distinctio
            eaque temporibus!
          </p>
        </div>

        <div class="box">
          <i class="fas fa-graduation-cap"></i>
          <span>2018</span>
          <h3>front end development</h3>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Obcaecati
            quos alias praesentium. Id autem provident laborum quae, distinctio
            eaque temporibus!
          </p>
        </div>

        <div class="box">
          <i class="fas fa-graduation-cap"></i>
          <span>2019</span>
          <h3>front end development</h3>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Obcaecati
            quos alias praesentium. Id autem provident laborum quae, distinctio
            eaque temporibus!
          </p>
        </div>

        <div class="box">
          <i class="fas fa-graduation-cap"></i>
          <span>2020</span>
          <h3>front end development</h3>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Obcaecati
            quos alias praesentium. Id autem provident laborum quae, distinctio
            eaque temporibus!
          </p>
        </div>

        <div class="box">
          <i class="fas fa-graduation-cap"></i>
          <span>2021</span>
          <h3>front end development</h3>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Obcaecati
            quos alias praesentium. Id autem provident laborum quae, distinctio
            eaque temporibus!
          </p>
        </div>
      </div>

-->
</section>
    <!-- education section ends -->

    <!-- portfolio section starts  -->

    {{-- <section class="portfolio" id="portfolio">
      <div class="containe">
      <div class="text"><h2>Student List</h2></div>
      </div>
      <table>
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Due Date</th>
            <th>Status</th>
            <th>Grade</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td><a href="#">101</a></td>
            <td>Smit V</td>
            <td>1/5/2021</td>
            <td>
              <p class="status status-unpaid">Past due</p>
            </td>
            <td class="amount">5</td>
          </tr>
          <tr>
            <td><a href="#">102</a></td>
            <td>Smit Vag</td>
            <td>1/4/2021</td>
            <td>
              <p class="status status-paid">Completed</p>
            </td>
            <td class="amount">4</td>
          </tr>
          <tr>
            <td><a href="#">103</a></td>
            <td>Smit Vagh</td>
            <td>1/2/2021</td>
            <td>
              <p class="status status-pending">Assigned</p>
            </td>
            <td class="amount">3</td>
          </tr>
          <tr>
            <td><a href="#">104</a></td>
            <td>Smit Vaghe</td>
            <td>12/30/2020</td>
            <td>
              <p class="status status-pending">Assigned</p>
            </td>
            <td class="amount">9</td>
          </tr>
          <tr>
            <td><a href="#">105</a></td>
            <td>Smit Vaghela</td>
            <td>12/18/2020</td>
            <td>
              <p class="status status-paid">Completed</p>
            </td>
            <td class="amount">7</td>
          </tr>
        </tbody>
      </table>
      <!--
      <h1 class="heading">my <span>portfolio</span></h1>

      <div class="box-container">
        <div class="box">
          <img src="images/img1.jpg" alt="" />
        </div>

        <div class="box">
          <img src="images/img2.jpg" alt="" />
        </div>

        <div class="box">
          <img src="images/img3.jpg" alt="" />
        </div>

        <div class="box">
          <img src="images/img4.jpg" alt="" />
        </div>

        <div class="box">
          <img src="images/img5.jpg" alt="" />
        </div>

        <div class="box">
          <img src="images/img6.jpg" alt="" />
        </div>
      </div>-->
    </section> --}}

    <!-- portfolio section ends -->

    <!-- contact section starts  -->
    {{-- <section class="contact" id="contact">
      <h1 class="heading"><span>Play</span> Quiz</h1>

      <div class="row">
        <div class="content">
          <h3 class="title">A Quick Trivia Quiz</h3>

          <div class="info">
            <a class="btn" href="game.html">Play</a>
          </div>
        </div>
      </section> --}}
      {{-- <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Give Question ID</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('questions.showById') }}" method="POST">
                    @csrf
                    <input type="text" name="searchById" class="" placeholder="Question ID" value="{{ request('searchById') }}">
                    <button type="submit"class="btn btn-success"><i class="fa fa-search"></i></button>
                </form>
            </div>
          </div>
        </div>
      </div> --}}

    <!-- contact section ends -->

    <!-- scroll top button  -->

    <a href="#home" class="top">
      <img src="images/scroll-top-img.png" alt="" />
    </a>

    <!-- jquery cdn link  -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

    <!-- custom js file link  -->
    <script src="{{ asset('js/dashboard_blade.js') }}"></script>
  </body>
</html>
