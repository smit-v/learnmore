<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Erudite</title>

    <!-- font awesome cdn link  -->
    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css"
    />

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <!-- custom css file link  -->
    <link rel="stylesheet" href="{{ asset('css/dashboard_blade.css') }}" />
    <style>
      .dropbtn {
        background-color: #04aa6d;
        color: white;
        padding: 16px;
        font-size: 16px;
        border: none;
      }

      .dropdown {
        position: relative;
        left: 85rem;
        top: 25px;
        display: inline-block;
      }

      .input-data{
          position: relative;
          margin: 4rem;
      }
      .dropdown-content {
        display: none;
        position: absolute;
        background-color: #f1f1f1;
        min-width: 160px;
        font-size: 14px;
        box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
        z-index: 1;
      }

      .dropdown-content a, .dropdown-content button{
        color: black;
        padding: 12px 16px;
        text-decoration: none;
        display: block;
        width: 100%;
        text-align: left;
      }

      .dropdown-content a:hover ,.dropdown-content button:hover{
        background-color: #ddd;
      }

      .dropdown:hover .dropdown-content {
        display: block;
      }

      .dropdown:hover .dropbtn {
        background-color: #3e8e41;
      }


      .home{
          padding: 0 1rem;
          display: block;
      }
    </style>
  </head>
  <body>
    <div class="dropdown">
      <button class="dropbtn">{{ auth()->user()->name }}</button>
      <div class="dropdown-content">
        {{-- <a href="#">My profile</a>
        <a href="#">Change password</a> --}}
        <form action="{{ route('logout') }}" method="POST" width="160px">
            @csrf
            <button type="submit">Logout</button>
        </form>
      </div>
    </div>
    <!-- header section starts  -->
    <header>
      <div class="user">
        <img src="{{ asset('img/logo.png') }}" alt="" />
        <!--
          <h3 class="name">Erudite</h3>
        <p class="post">The Unified Platform</p>-->
      </div>

      <nav class="navbar">
        <ul>
          <li><a href="{{ route('dashboard') }}">home</a></li>

          <li><a href="{{ route('quiz.teacher.manageQuiz', auth()->user()->id) }}">Manage Quizes</a></li>
        </ul>
      </nav>
    </header>
    <!-- header section ends -->

    <!-- form section -->

        <section class="about" id="about">
            <div class="containe">
                <div class="text"><h2>Quiz Create</h2></div>
                <form action="{{ route('quiz.teacher.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                   <div class="form-row">
                    @if ($errors)
                        <ul>
                            @foreach ($errors->all() as $message)
                                <li>{{ $message }}</li>
                            @endforeach
                        </ul>
                    @endif
                   </div>


                    <div class="input-data">
                         <input type="text" name='title' value="{{ old('title') }}" required>
                         <div class="underline"></div>
                         <label for="">Quiz name</label>
                      </div>

                      <div class="input-data">

                        <input type="date" name="date" value="{{ old('date') }}"required>
                        <div class="underline"></div>

                     </div>


                   <div class="input-data">

                    <input type="time" name="time" value="{{ old('time') }}" required>
                    <div class="underline"></div>

                   </div>

                    <div class="input-data">

                        <input type="number" name="outoff_marks" value="{{ old('outoff_marks') }}" required>
                        <div class="underline"></div>
                        <label for="outoff_marks">Outoff Marks</label>
                     </div>

                     <div class="input-data">

                        <input type="number" name="duration" value="{{ old('duration') }}" required min="0">
                        <div class="underline"></div>
                        <label for="duration">Duration of quiz</label>
                     </div>

                     <div class="input-data">

                        <select name="results" required>
                            <option value="YES">YES</option>
                            <option value="NO">NO</option>
                        </select>
                        <div class="underline"></div>
                        <label for="results">Show Results</label>
                     </div>


                    <div class="form-row submit-btn">
                    <div class="input-data">
                        <div class="inner"></div>
                        <input type="submit" value="submit">
                    </div>
                    </div>
                </form>
             </div>
            </section>

    <!-- /form section -->

    <!-- jquery cdn link  -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <!-- custom js file link  -->
    <script src="{{ asset('js/dashboard_blade.js') }}"></script>
    <script>
        $('.select2').select2({
        placeholder: 'Select a Teacher',
        allowClear: true,
        maximumSelectionLength: 1
    });
    </script>
  </body>
</html>
