@extends('layouts.app')
@section('page-level-styles')

@endsection
@section('content')
<div class="container">
    <a class="mb-3 btn btn-primary" href="{{ route('quiz.teacher.manageQuiz', $quiz->assigned_by) }}">
        Back
    </a>
    <div class="row justify-content-center">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header">
                    <h1 class="pull-left">{{ $quiz->title }} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Code:{{ $quiz->quizcode }}</h1>
                    <p class="float-right">Last Updated - {{ $quiz->updated_at }}</p>

                </div>
                <div class="card-body">
                    @if ($errors)
                    <ul>
                        @foreach ($errors->all() as $message)
                            <li class="text-danger">{{ $message }}</li>
                        @endforeach
                    </ul>
                    @endif

                    <form action="{{ route('quiz.teacher.update.quiz', [$quiz->assigned_by, $quiz->id]) }}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="title">Title</label>
                                <input type="text" class="form-control" name="title" id=title value="{{ old('title', $quiz->title) }}" required>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="quizcode">Quizcode</label>
                                <input type="text" readonly class="form-control" name="quizcode" id=quizcode value="{{ old('quizcode', $quiz->quizcode) }}" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="date">Submission Date</label>
                                <input id="date" type="date" class="form-control" name="date" value="{{ old('date', $quiz->submission_date) }}"required>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="time">Submission Time</label>
                                <input id="time" type="time" class="form-control" name="time" value="{{ old('time',  $quiz->submission_time) }}"required>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="duration">Duration (in minutes)</label>
                                <input id="duration" type="number" class="form-control" name="duration" value="{{ old('duration', $quiz->duration) }}"required>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="marks">Total Marks</label>
                                <input id="marks" type="marks" class="form-control" name="marks" value="{{ old('marks', $quiz->outoff_marks) }}"required>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="show_results">View Result</label>
                                <select class="form-control" name="show_results" id="show_results">
                                    <option value="YES"
                                        @if($quiz->show_results == "YES") selected @endif
                                    >YES</option>
                                    <option value="NO"
                                        @if($quiz->show_results == "NO") selected @endif
                                    >NO</option>
                                </select>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-warning">Edit</button>
                        <a href="{{ route('quiz.teacher.questionshow', $quiz->id) }}" class="btn btn-primary text-light">Add / Delete Questions</a>
                    </form>
                </div>
                <div class="card-footer">
                    <div class="d-flex justify-content-between">
                        <div class="d-flex">
                            <form action="{{ route('quiz.teacher.delete',[$quiz->assigned_by,$quiz->id]) }}" method="POST" class="mr-3 pull-right">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete it?')">Delete quiz</button>
                            </form>
                        </div>
                        <div class="d-flex flex-column">
                            <div class="text-muted mb-2 text-right">
                                <p>Submission Date - {{ $quiz->submission_date }}</p>
                            </div>
                            <div class="text-muted mb-2 text-right">
                                <p>Submission Time - {{ $quiz->submission_time }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card mt-4">
                <div class="card-header">
                    <h3 class="pull-left">Student List</h3>
                    <a href="" id="excel_button" class="btn btn-success pull-right">Download Excel</a>
                </div>
                <div class="card-body">

                    <table class="table table-hover" id="student_table">
                        <thead>
                            <tr>
                              <th scope="col">#</th>
                              <th scope="col">Name</th>
                              <th scope="col">Status</th>
                              <th scope="col">Marks</th>
                              <th scope="col">Actions</th>
                            </tr>
                          </thead>

                          @foreach ($quiz->quizattempts as $attempt)
                          <tr>
                            <th scope="row">{{ $loop->index+1 }}</th>

                                <td>{{ $attempt->student->name }}</td>
                                <td>{{ $attempt->status }}</td>

                                @if ($attempt->marks)
                                    <td>{{ $attempt->marks }} / {{ $attempt->quiz->outoff_marks }}</td>
                                @else
                                    <td>Marks not alloted</td>

                                @endif

                                <td>
                                    <form action="{{ route('quiz.teacher.re-exam') }}" method="POST">
                                        @csrf
                                        <input type="hidden" name="attempt_id" value="{{ $attempt->id }}">
                                        <button type="submit" class="btn btn-primary">Allow Re-Exam</button>
                                    </form>
                                    {{-- <button class="btn btn-primary">Delete Attempt</button> --}}
                                </td>

                          </tr>
                            @endforeach
                    </table>


                </div>
                <div class="card-footer">
                    <div class="d-flex justify-content-between">
                        <div class="d-flex">

                        </div>
                        <div class="d-flex flex-column">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection
@section('page-level-scripts')
<script type="text/javascript" src="https://unpkg.com/xlsx@0.15.1/dist/xlsx.full.min.js"></script>
<script>
    function html_table_to_excel(type)
    {
        var data = document.getElementById('student_table');

        var file = XLSX.utils.table_to_book(data, {sheet: "sheet1"});

        XLSX.write(file, { bookType: type, bookSST: true, type: 'base64' });

        XLSX.writeFile(file, 'file.' + type);
    }

    const export_button = document.getElementById('excel_button');

    export_button.addEventListener('click', () =>  {
        html_table_to_excel('xlsx');
    });
</script>
@endsection


