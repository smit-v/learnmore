<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Erudite</title>

    <!-- font awesome cdn link  -->
    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css"
    />

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <!-- custom css file link  -->
    <link rel="stylesheet" href="{{ asset('css/dashboard_blade.css') }}" />
    <style>
      .dropbtn {
        background-color: #04aa6d;
        color: white;
        padding: 16px;
        font-size: 16px;
        border: none;
      }

      .dropdown {
        position: relative;
        left: 85rem;
        top: 25px;
        display: inline-block;
      }

      .dropdown-content {
        display: none;
        position: absolute;
        background-color: #f1f1f1;
        min-width: 160px;
        font-size: 14px;
        box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
        z-index: 1;
      }

      .dropdown-content a, .dropdown-content button{
        color: black;
        padding: 12px 16px;
        text-decoration: none;
        display: block;
        width: 100%;
        text-align: left;
      }

      .dropdown-content a:hover ,.dropdown-content button:hover{
        background-color: #ddd;
      }

      .dropdown:hover .dropdown-content {
        display: block;
      }

      .dropdown:hover .dropbtn {
        background-color: #3e8e41;
      }


      .home{
          padding: 0 1rem;
          display: block;
      }
    </style>
  </head>
  <body>
    <div class="dropdown">
      <button class="dropbtn">{{ auth()->user()->name }}</button>
      <div class="dropdown-content">
        {{-- <a href="#">My profile</a>
        <a href="#">Change password</a> --}}
        <form action="{{ route('logout') }}" method="POST" width="160px">
            @csrf
            <button type="submit">Logout</button>
        </form>
      </div>
    </div>
    <!-- header section starts  -->
    <header>
      <div class="user">
        <img src="{{ asset('img/logo.png') }}" alt="" />
        <!--
          <h3 class="name">Erudite</h3>
        <p class="post">The Unified Platform</p>-->
      </div>

      <nav class="navbar">
        <ul>
          <li><a href="{{ route('quiz.teacher.home') }}">home</a></li>

          <li><a href="{{ route('quiz.teacher.manageQuiz', auth()->user()->id) }}">Manage Quizes</a></li>
        </ul>
      </nav>
    </header>
    <!-- header section ends -->

    <!-- form section -->

        <p>Your Quiz Code is: <h2>{{ $quiz->quizcode }}</h2></p>
        <a href="{{ route('quiz.teacher.home') }}" class="btn btn-primary">Done</a>
       <p>Selected questions</p>
        <ul>
            @foreach ($quizQs as $quizQ)
                <li>{{ $quizQ->question }}</li>
            @endforeach
        </ul>

    <!-- /form section -->

    <!-- home section starts  -->
    <section class="home" id="home">
        <div class="container">
            <a class="btn btn-primary" href="{{ route('quiz.teacher.question.create') }}">Add a Question</a>
           <div class="questions">
            @foreach ($quizQuestions as $quizQuestion)
            <div class="card">
                <div class="row">
                    <div class="col-9 question-details">
                        <h3>{{ $quizQuestion->question }}</h3>
                        <div class="d-flex justify-content-between">
                            <ul>
                                <li>{{ $quizQuestion->option1 }}</li>
                                <li>{{ $quizQuestion->option2 }}</li>
                                <li>{{ $quizQuestion->option3 }}</li>
                                <li>{{ $quizQuestion->option4 }}</li>
                                <li class="text-success">Answer: {{ $quizQuestion->answer }}</li>
                            </ul>

                            <p>Created by: {{ $quizQuestion->questionedby->name }}</p>
                        </div>
                    </div>
                    <div class="col-3 question-controls">
<?php
    $quizQSS = $quizQs->pluck('id');
?>
                        @if (!$quizQSS->contains($quizQuestion->id))
                            <form action="{{ route('quiz.teacher.upQuestion', $quiz->id) }}" method="POST">
                                @csrf
                                <input type="hidden" name='quizquestion_id' value="{{ $quizQuestion->id }}">
                                <button type="submit" class="btn btn-primary">Add question</button>
                            </form>
                        @else
                            <form action="{{ route('quiz.teacher.downQuestion', $quiz->id) }}" method="POST">
                                @csrf
                                <input type="hidden" name='quizquestion_id' value="{{ $quizQuestion->id }}">
                                <button type="submit" class="btn btn-primary">Addedd</button>
                            </form>
                        @endif

                        <a class="btn btn-warning" href="{{ route('quiz.teacher.question.edit', $quizQuestion->id) }}">Edit</a>
                        <form action="{{ route('quiz.teacher.question.delete',$quizQuestion->id) }}" method="POST" class="inline">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete it?')">Delete</button>
                        </form>
                    </div>
                </div>
            </div>
            @endforeach
           </div>
        </div>
      </section>

    <!-- home section ends -->


    <!-- Student Quiz Modal -->

    <!-- jquery cdn link  -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <!-- custom js file link  -->
    <script src="{{ asset('js/dashboard_blade.js') }}"></script>
    <script>
        $('.select2').select2({
        placeholder: 'Select a Teacher',
        allowClear: true,
        maximumSelectionLength: 1
    });
    </script>
  </body>
</html>
