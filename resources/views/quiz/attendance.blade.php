<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>Erudite</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

  <script defer src="{{ asset('face_recog/face-api.min.js') }}"></script>
  <script defer src="{{ asset('face_recog/script.js') }}"></script>
  <style>
    body {
      margin: 0;
      padding: 0;
      width: 100vw;
      height: 100vh;
      display: flex;
      justify-content: center;
      align-items: center;
    }

    canvas {
      position: absolute;
    }
  </style>
</head>
<body>
  <video id="video" width="720" height="560" autoplay muted></video>
  <input type="hidden" value="{{ $ids }}" id="userids">
                            {{-- <input type="hidden" value="@foreach($ids as $ide)
                            @if($loop->index == 0)'{{$ide}}'
                            @else,'{{$ide}}'
                            @endif
                        @endforeach" id="userids"> --}}
  <input type="hidden" value="{{ $qa->id }}" id="qaid">
  <a href="{{ route('quiz.start', $qa->id) }}" class="btn btn-primary" id="startBTN">Start Test</a>

{{-- <script type="module">
    // import { parse } from 'node-html-parser';
    // var faceapi = require('{{ asset('face_recog/face-api.min.js') }}');
    import * as faceapi from '{{ asset('face_recog/face-api.min.js') }}';


    var faceapii = "{{ asset('face_recog/face-api.min.js') }}";
    console.log(faceapi.tinyFaceDetector);
Promise.all([

  faceapi.nets.tinyFaceDetector.loadFromUri('{{ asset('face_recog/models') }}'),
  faceapi.nets.faceLandmark68Net.loadFromUri('{{ asset('face_recog/models/faceLandmark68Net') }}'),
  faceapi.nets.faceRecognitionNet.loadFromUri('{{ asset('face_recog/models/faceRecognitionNet') }}'),
  faceapi.nets.ssdMobilenetv1.loadFromUri('{{ asset('face_recog/models/ssdMobilenetv1') }}'),
]).then(startVideo)

function startVideo() {
  navigator.getUserMedia(
    { video: {} },
    stream => video.srcObject = stream,
    err => console.error(err)
  )
}

video.addEventListener('play', () => {
  const canvas = faceapi.createCanvasFromMedia(video)
  document.body.append(canvas)
  const displaySize = { width: video.width, height: video.height }
  faceapi.matchDimensions(canvas, displaySize)

  let labeledFaceDescriptors
  (async () => {
    labeledFaceDescriptors = await loadLabeledImages()
  })()

  MyInterval = setInterval(async () => {
    const detections = await faceapi.detectAllFaces(video, new faceapi.TinyFaceDetectorOptions()).withFaceLandmarks().withFaceDescriptors()
    const resizedDetections = faceapi.resizeResults(detections, displaySize)
    canvas.getContext('2d').clearRect(0, 0, canvas.width, canvas.height)

    if (labeledFaceDescriptors) {
      const faceMatcher = new faceapi.FaceMatcher(labeledFaceDescriptors, 0.6)
      const results = resizedDetections.map(d => faceMatcher.findBestMatch(d.descriptor))
      results.forEach((result, i) => {
        const box = resizedDetections[i].detection.box
        const drawBox = new faceapi.draw.DrawBox(box, { label: result.toString() })
        drawBox.draw(canvas)

        if(!result.toString().includes('unknown'))
        {
          stop(result.label);
        }
      })
    }

  }, 500)
})

function stop(user)
{
  clearInterval(MyInterval);
  console.log(user);

//   $.ajaxSetup({
//     headers: {
//         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//     }
//     });
//     $.ajax({
//         type: "POST",
//         url: '{{ route("quiz.markAttendance", $qa->id) }}',

//         data: {user: "{{ $qa->student->id }}"},
//         success: function( data ) {
//             $("#ajaxResponse").innerHTML = data ;
//             console.log(data);
//         },
//         statusCode: {
//             200: function(){
//                      console.log("200")
//                  },
//             500: function(){
//                      console.log("500")
//                  }
//         }
//     });
}

function loadLabeledImages() {
  const labels = [
      @foreach ($ids as $ide)
            @if ($loop->index == 0)
                '{{ $ide }}'
            @else
                ,'{{ $ide }}'
            @endif
      @endforeach
  ];
  return Promise.all(
    labels.map(async label => {
      const descriptions = []
      for (let i = 1; i <= 2; i++) {
        // const img = await faceapi.fetchImage(`https://raw.githubusercontent.com/naitik2314/Face-Recognition-JavaScript/master/labeled_images/${label}/${i}.jpg`)
        const img = await faceapi.fetchImage(`http://localhost:8000/storage/users/${label}/${i}.jpg`)
        // const img = await faceapi.fetchImage( asset() )
        const detections = await faceapi.detectSingleFace(img).withFaceLandmarks().withFaceDescriptor()
        descriptions.push(detections.descriptor)
      }

      return new faceapi.LabeledFaceDescriptors(label, descriptions)
    })
  )
}

</script> --}}
</body>
</html>
