<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>Erudite</title>
    <style>
        #video-div{
            position: absolute;
            top:0;
            right:0;
            width: 20%;
            height: 40%;
            z-index: 99;
            padding: 0.5rem;
        }
         canvas{
             position: absolute;
             top:0;
             right: 2.5%;
             z-index: 100;
         }

         #video_message{
             background: white;
             border: 1px solid black;
             border-radius: 5px;
             padding: 0.5rem;
         }

    </style>
</head>
<body>
{{-- Navbar --}}
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand text-light">Erudite</a>
{{-- /Navbar --}}
</nav>

<div id="video-div">
    <video id="video" width="250" height="150" autoplay muted></video>
    <p class="text-danger" id="video_message"></p>
</div>

<div id="card-set" class="d-flex justify-content-center ">
    <div class="card mt-5 w-75">
        <div class="card-header bg-dark text-light">
          <p class="font-weight-bold m-0">MCQs</p>
          <p class="font-weight-bold m-0" id="timer"></p>
        </div>
        <div class="card-body">
            <fieldset class="form-group">
                  <div class="row">
                    <div class="col-md-5">
                        <input id="word-id" type="hidden" value="">
                        <h1 id="show-word" class="text-capitalize"></h1>
                    </div>
                    <div class="col-sm-7">

                      @for($j=0; $j<4; $j++)
                      <div class="form-check">
                        <input id="radio{{$j}}" class="form-check-input" type="radio" name="radioans" value="">
                        <label class="form-check-label" for="radio{{$j}}">
                            <p id="show-option{{$j}}" class="text-capitalize"></p>
                        </label>
                      </div>
                      @endfor

                    </div>
                  </div>
            </fieldset>
        </div>
      </div>
</div>


<div id="controls-set">
    <div class="mt-5 d-flex justify-content-center align-content-center">
        <button id="btn-prev" onclick="prevQuestion()" class="btn btn-dark mr-2">Previous</button>
        <button id="btn-submit" onclick="saveAns()" class="btn btn-primary mr-2">Submit</button>
        <button id="btn-clear" onclick="clearAns()" class="btn btn-primary mr-2">Clear</button>
        <button id="btn-next" onclick="nextQuestion()" class="btn btn-dark m-0">Next</button>
      </div>
      <p id="page-ans" class="mt-3 text-center"></p>
      <div class="text-center">
          @foreach ($quizQuestions as $question)
            <button class="btn btn-warning ml-2" id="navQuestion{{ $loop->index }}" onclick="navQuestion({{ $loop->index }})"></button>
          @endforeach
      </div>
      <div class="text-center">
          <button id="btn-submit-Test" onclick="submitTest()" class="btn btn-success mt-5">Submit Test</button>
      </div>
</div>


<footer class="">
    <!-- Copyright -->
<div class="text-center p-4" style="background-color: rgba(0, 0, 0, 0.05);">
 © 2021 Copyright
    <p class="font-weight-bold" style="display:inline">Smit Vaghela</p>
</div>
<div id="ajaxResponse">

</div>
<!-- Copyright -->
</footer>
{{-- /Footer --}}


<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script>
    var distance = {{ $time}}; // 60 se multiply hoke ayega
    var timerJS = setInterval(function()
    {
        var minutes = Math.floor(distance/60);
        var seconds = distance%60;
        document.getElementById("timer").innerHTML = minutes+" : "+seconds + "m";
        if(distance <= 120)
        {
            document.getElementById("timer").classList.add("text-danger");
        }
        if (distance < 0)
        {
            document.getElementById("timer").innerHTML = "EXPIRED";
            submitTest();
        }
        distance--;

        $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
        });
        $.ajax({
            type: "PUT",
            url: '{{ route('quiz.student.update.time') }}',

            data: {_method:'PUT', 'startTime': distance, qa: "{{ $q->id }}"},
            success: function( data ) {
                $("#ajaxResponse").innerHTML = data ;
                console.log(data);
            },
            statusCode: {
                200: function(){
                         console.log("200")
                     },
                500: function(){
                         console.log("500")
                     }
            }
        });

    }, 1000);

    questions = [];
    questions_options = [];
    ansPaper = [];


    @foreach ($quizQuestions as $quizQuestion)
        questions.push("{{ $quizQuestion->question }}");

        options = [];
        options.push("{{ $quizQuestion->option1 }}");
        options.push("{{ $quizQuestion->option2 }}");
        options.push("{{ $quizQuestion->option3 }}");
        options.push("{{ $quizQuestion->option4 }}");
        ans = "{{ $quizQuestion->answer }}";
        ansPaper.push(ans);
        questions_options.push(options);

    @endforeach

    savedAns = new Array(ansPaper.length).fill(0);
    currentofQuestions = -1;
    showQuestion(1);

    function nextQuestion()
    {
        showQuestion(1);
        checkSelectedAns();
    }

    function prevQuestion()
    {
        showQuestion(-1);
        checkSelectedAns();
    }

    function saveAns()
    {
        var ans = $("input[name='radioans']:checked").val();
        if(ans == null)
        {
            alert('Please select an option');
            return;
        }
        savedAns[currentofQuestions] = ans;
        $("#navQuestion"+currentofQuestions).addClass('btn-success');
        $("#navQuestion"+currentofQuestions).removeClass('btn-warning');
    }

    function clearAns()
    {
        $("input[name=radioans]:checked").prop('checked', false);
        savedAns[currentofQuestions] = 0;
        $("#navQuestion"+currentofQuestions).removeClass('btn-success');
        $("#navQuestion"+currentofQuestions).addClass('btn-warning');
    }

    function navQuestion(n)
    {
        currentofQuestions = n-1;
        showQuestion(1);
        checkSelectedAns();
    }

    function showResults()
    {

    }

    function showEndPage()
    {
        settingHTML = `<div class="card mt-5 w-100">
                        <div class="card-header bg-dark text-light">
                                <p class="font-weight-bold m-0">Results</p>
                            </div>

                        <div class="card-body">
                            Thankyou for your quiz attempt.
                        </div>
                       </div>
            `;
        controlbtn = `<div class="d-flex justify-content-center">
                        <a class="btn btn-primary mt-5" href="`+"{{ route("dashboard") }}"+`">Back to Home</a>
                    </div>`;
        $("#controls-set").html(controlbtn);
        $("#card-set").html(settingHTML);
    }

    function submitTest()
    {
        clearInterval(timerJS);

        var now = new Date();
        var db = new Date('{{ $quiz->submission_date." ".$quiz->submission_time }}');

        if(now.getTime() >= db.getTime())
        {
            controlbtn = `<div class="d-flex justify-content-center">
                        <a class="btn btn-primary mt-5" href="`+"{{ route("dashboard") }}"+`">Back to Home</a>
                    </div>`;
            $("#controls-set").html(controlbtn);
            settingHTML = `<div class="card mt-5 w-100">
                        <div class="card-header bg-dark text-light">
                                <p class="font-weight-bold m-0">Results</p>
                            </div>

                        <div class="card-body">
                            Quiz submission time expired
                        </div>
                       </div>`
            $("#card-set").html(settingHTML);


            $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
        });
        $.ajax({
            type: "PUT",
            url: '{{ route('quiz.late') }}',

            data: {_method:'PUT', qa: "{{ $q->id }}"},
            success: function( data ) {
                $("#ajaxResponse").innerHTML = data ;
                console.log(data);
            },
            statusCode: {
                200: function(){
                         console.log("200")
                     },
                500: function(){
                         console.log("500")
                     }
            }
        });

            return;
        }

        notAnswered = 0;
        rightAnswered = 0;
        wrongAnswered = 0;
        for(i=0; i<savedAns.length; i++)
        {
            if(savedAns[i] != 0)
            {
                if(savedAns[i] == ansPaper[i])
                {
                    rightAnswered++;
                }
                else
                {
                    wrongAnswered++;
                }
            }
            else
            {
                notAnswered++;
            }
        }
        settingHTML = `

            <div class="card mt-5 w-100">
                <div class="card-header bg-dark text-light">
                    <p class="font-weight-bold m-0">Results</p>
                </div>

                <div class="card-body">
                    <div class="d-flex">
                        <div class="col-4 text-center">
                            <h2 class="text-success">Correct</h2>
                            <h2 class="text-success display-3">`+ rightAnswered +`</h2>
                        </div>
                        <div class="col-4 text-center">
                            <h2 class="text-warning">Not Answered</h2>
                            <h2 class="text-warning display-3">`+ notAnswered +`</h2>
                        </div>
                        <div class="col-4 text-center">
                            <h2 class="text-danger">Wrong</h2>
                            <h2 class="text-danger display-3">`+ wrongAnswered +`</h2>
                        </div>
                    </div>
                    <div class="d-flex mt-5">
                        <table class="table table-hover">
                        <thead class="thead">
                            <tr>
                            <th scope="col">#</th>
                            <th scope="col">Word</th>
                            <th scope="col">Your Selection</th>
                            <th scope="col">Answer</th>
                            </tr>
                        </thead>
                        <tbody>


        `;

        tablesHTML = ``;



        for(i=0; i<ansPaper.length; i++)
        {
            questionNumber = i+1;
            question_check = questions[i];
            correct_check = ansPaper[i];
            ans = savedAns[i];


            if(ans)
            {

                if(ans == correct_check)
                {
                    your_check = `<td class="text-success">`+ ans +`</td>`;
                }
                else
                {
                    your_check = `<td class="text-danger">`+ ans +`</td>`;
                }
            }
            else
            {
                your_check = `<td class="text-warning"> Not Answered </td>`;
            }
            tablesHTML = tablesHTML + `
                        <tr>
                            <th scope="row">`+ questionNumber +`</th>
                            <th>`+ question_check +`</th>`
                            + your_check +

                            `<td class="text-success">`+ correct_check +`</td>
                        </tr>
            `;
        }

        settingHTML  = settingHTML+ tablesHTML +`
                                </tbody>
                        </table>
            </div>
            </div>
        </div>
        `;
        var totalMarks = {{ $quiz->outoff_marks }};
        var numberOfquestions = ansPaper.length;
        var marks_earned = (Math.floor(totalMarks/numberOfquestions)) * rightAnswered;


        controlbtn = `<div class="d-flex justify-content-center">
                        <a class="btn btn-primary mt-5" href="`+"{{ route("dashboard") }}"+`">Back to Home</a>
                    </div>`;
        $("#controls-set").html(controlbtn);

        settingHTML2 = `<div class="card mt-5 w-100">
                        <div class="card-header bg-dark text-light">
                                <p class="font-weight-bold m-0">Results</p>
                            </div>

                        <div class="card-body">
                            Thankyou for your quiz attempt.
                        </div>
                       </div>
            `;





        @if($quiz->show_results == "YES")
            $("#card-set").html(settingHTML);
        @else
            $("#card-set").html(settingHTML2);
        @endif

        $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
        });
        $.ajax({
            type: "POST",
            url: '{{ route("quiz.end") }}',

            data: {quiz_id: "{{ $quiz->id }}", user_id: "{{ $user->id }}", marks: marks_earned, qa: "{{ $q->id }}"},
            success: function( data ) {
                $("#ajaxResponse").innerHTML = data ;
                console.log(data);
            },
            statusCode: {
                200: function(){
                         console.log("200")
                     },
                500: function(){
                         console.log("500")
                     }
            }
        });
    }

    function showQuestion(val)
    {
        if(val == 1)
        {

            if(questions.length-1 <= currentofQuestions)
            {
                return;
            }
            currentofQuestions++;
        }
        else if(val == -1)
        {
            if(0 >= currentofQuestions)
            {
                return;
            }
            currentofQuestions--;
        }

        $("#word-id").attr("value",ansPaper[currentofQuestions]);
        $("#show-word").text(questions[currentofQuestions]);
        for(i=0; i<4; i++)
        {
            $("#radio"+i).attr("value",questions_options[currentofQuestions][i]);
            $("#show-option"+i).text(questions_options[currentofQuestions][i]);
        }

    }

    function checkSelectedAns()
    {
        if(savedAns[currentofQuestions] != 0)
        {
            check = ""+savedAns[currentofQuestions];
            $("input[value="+check+"]").prop('checked', true);
        }
        else
        {
            $("input[name=radioans]:checked").prop('checked', false);
        }
    }

</script>

  <script defer src="{{ asset('face_recog/face-api.min.js') }}"></script>
  <script defer src="{{ asset('face_recog/script2.js') }}"></script>

</body>
</html>
