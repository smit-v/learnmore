<?php

namespace Database\Seeders;

use App\Models\quiz;
use App\Models\quizquestion;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::factory(10)->create();
        User::create([
            'name' => 'Student Admin',
            'email' => 'admin@admin.com',
            'organization' => 'SS',
            'type' => 'Student',
            'password' => Hash::make("adminadmin"),
            'approved' => "YES"
        ]);

        User::create([
            'name' => 'Teacher Admin',
            'email' => 'admin@admin1.com',
            'organization' => 'SS',
            'type' => 'Teacher',
            'approved' => "YES",
            'password' => Hash::make("adminadmin"),
        ]);

        User::create([
            'name' => 'Admin',
            'email' => 'admin@admin11.com',
            'organization' => 'SS',
            'type' => 'Admin',
            'approved' => "YES",
            'password' => Hash::make("adminadmin"),
        ]);

        quizquestion::create([
            'question' => 'What is Love ?',
            'option1' => 'Car',
            'option2' => 'Gift',
            'option3' => 'Feeling',
            'option4' => 'Computer',
            'answer' => 'Computer',
            'question_by' => 1
        ]);

        quizquestion::create([
            'question' => 'What is My Name ?',
            'option1' => 'Shyam',
            'option2' => 'Ram',
            'option3' => 'Shyama',
            'option4' => 'Doe',
            'answer' => 'Ram',
            'question_by' => 6
        ]);

        quizquestion::create([
            'question' => 'Where Do I stay ?',
            'option1' => 'Mumbai',
            'option2' => 'Chennai',
            'option3' => 'Delhi',
            'option4' => 'Bangalore',
            'answer' => 'Chennai',
            'question_by' => 5
        ]);

        quizquestion::create([
            'question' => 'What is this?',
            'option1' => 'Water',
            'option2' => 'Fire',
            'option3' => 'Aire',
            'option4' => 'Land',
            'answer' => 'Land',
            'question_by' => 5
        ]);

        quizquestion::create([
            'question' => 'How old are you?',
            'option1' => '9',
            'option2' => '11',
            'option3' => '23',
            'option4' => '10',
            'answer' => '9',
            'question_by' => 6
        ]);

        $q = quiz::create([
            'assigned_by' => 1,
            'quizcode' => 'WEVBJ067',
            'outoff_marks' => 20,
            'title' => '1st Test'
        ]);

        $q->quizquestion()->attach(quizquestion::find(1));

    }
}
